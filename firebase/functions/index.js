const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

var db = admin.firestore();

//exports.createShareholderRequest = functions.firestore
//    .document('holdersRequest/{request}')
//    .onCreate((snap, context) => {
//      var owner = snap.data()['token'];
//
//      var payload = {
//               data: {
//                 type: "holder_added",
//                 holderId: snap.data()['holderId'],
//                 id: snap.data()['id'],
//                 part: snap.data()['part'].toString(),
//                 holderName: snap.data()['holderName'],
//                 holderToken: snap.data()['holderToken']
//                },
//                notification: {
//                  title: "Shareholder added",
//                  body: "Shareholder added successfully"
//                }
//             };
//
//             admin.messaging().sendToDevice(owner, payload)
//                .then(function(response) {
//                   console.log("Successfully sent message:", response);
//                    return;
//                 })
//                 .catch(function(error) {
//                   console.log("Error sending message:", error);
//                   return;
//                 });
//                 db.collection('holdersRequest').doc(snap.id).delete();
//         return;
//    });

    exports.createShareholder = functions.firestore
        .document('shareholders/{holder}')
        .onCreate((snap, context) => {
        var holder = snap.data()['holderToken'];
        var id = snap.data()['id'];

                 var payload = {
                          data: {
                            type: "owner_approved",
                            id: id,
                            infoName: snap.data()['infoName'],
                            ownerName: snap.data()['ownerName']
                           },
                           notification: {
                             title: "Approved",
                             body: "Info owner approved"
                           }
                        };

                        admin.messaging().sendToDevice(holder, payload)
                           .then(function(response) {
                              console.log("Successfully sent message:", response);
                               return;
                            })
                            .catch(function(error) {
                              console.log("Error sending message:", error);
                              return;
                            });


                            db.collection('infos').doc(id).get()
                            .then(doc => {

                            if(doc.data().status === 'Not shared'){
                            db.collection('infos').doc(id).update({status: 'Hold'});



//                            var payload = {
//                             data: {
//                               type: "status_changed",
//                               id: id,
//                               status: 'Hold'
//                              }
//                           };
//
//                           admin.messaging().sendToDevice(doc.data()['ownerToken'], payload)
//                              .then(function(response) {
//                                 console.log("Successfully sent message:", response);
//                                  return;
//                               })
//                               .catch(function(error) {
//                                 console.log("Error sending message:", error);
//                                 return;
//                               });


                            }
                            return;
                            }).catch(function(error) {
                              console.log("Error getting documents: ", error);
                            });
              return;
        });

exports.createInfoRequest = functions.firestore
    .document('infoRequest/{request}')
    .onCreate((snap, context) => {
       var tokens = snap.data()['token'];
       var id = snap.data()['id'];
       var infoName = snap.data()['infoName'];
       var infoOwner = snap.data()['ownerName'];

//var batch = db.batch();
//       db.collection('shareholders').where('id','==',id).get()
//       .then(snapshot3=>{
// snapshot3.forEach(documentSnapshot => {
//   batch.update(documentSnapshot.ref,{status: 3});
//      console.log(doc.id, '=>', doc.data());
//    });
//    batch.commit();
//    return;
//       })
//     .catch(function(error) {
//                          console.log("Error getting documents: ", error);
//                      });
//
//       db.collection('shareholders').where('id','==',id).get()
//        .then(querySnapshot => {
//        var holders = [];
//        var query = db.collection('users');
//               querySnapshot.forEach(doc => {
//                   // doc.data() is never undefined for query doc snapshots
////                   holders.push(doc.data()['holderId']);
//                   query.where('id','==',doc.data()['holderId']);
//                   console.log(doc.id, " => ", doc.data());
//               });
//
//               var tokens = [];
//               db.collection('users').get()
//               .then(querySnapshot2 => {
//
//               querySnapshot2.forEach(doc2 => {
//                  tokens.push(doc2.data()['token']);
//               });

              var payload = {
                 data: {
                   type: "owner_requested",
                   id: id
                  },
                  notification: {
                    title: "Request information",
                    body: infoOwner + " requested his " + infoName + " info part from you"
                  }
                  };

               admin.messaging().sendToDevice(tokens, payload)
                  .then(function(response) {
                     console.log("Successfully sent message:", response);
                      return;
                   })
                   .catch(function(error) {
                     console.log("Error sending message:", error);
                     return;
                   });
                   db.collection('infoRequest').doc(snap.id).delete();
    return;
});

exports.updateShareholder = functions.firestore
    .document('shareholders/{holder}')
    .onUpdate((change, context) => {

      const newValue = change.after.data();

      const previousValue = change.before.data();

      if(previousValue.status !== newValue.status && previousValue.status !== 'Hold' && previousValue.status !== 'Deny'){
         db.collection('infos').doc(newValue.id).update({status: "Need update"});

         db.collection('infos').doc(newValue.id).get()
           .then(doc => {

           var payload = {
               data: {
                 type: "status_changed",
                 part: newValue.part.toString(),
                 id: newValue.id,
                 status: newValue.status
                 }
                };

             admin.messaging().sendToDevice(doc.data()['ownerToken'], payload)
                .then(function(response) {
                   console.log("Successfully sent message:", response);
                    return;
                 })
                 .catch(function(error) {
                   console.log("Error sending message:", error);
                   return;
                 });


           return;
      }).catch(function(error) {
               console.log("Error sending message:", error);
               return;
             });

}
    return
    });