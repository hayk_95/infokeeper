package com.infokeeper.infokeeper;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.infokeeper.infokeeper.activities.MainActivity;
import com.infokeeper.infokeeper.data.DataRepository;
import com.infokeeper.infokeeper.data.enums.InfoStatus;
import com.infokeeper.infokeeper.data.models.Shareholder;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

public class FirebaseService extends FirebaseMessagingService {

    private static final String CHANNEL_ID = "notificationChannelId";
    private static int notificationId = 1;

    @Override
    public void onNewToken(String token) {
        DataRepository.getInstance(getApplicationContext()).saveToken(token);
        DataRepository.getInstance(getApplicationContext()).updateRegistrationToken(token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData() != null && remoteMessage.getData().get(Constants.MESSAGE_TYPE) != null) {
            if ((remoteMessage.getData().get(Constants.MESSAGE_TYPE)).equals(Constants.SHAREHOLDER_ADDED)) {
                Shareholder shareholder = new Shareholder(remoteMessage.getData().get(Constants.SHAREHOLDER_ID),
                        remoteMessage.getData().get(Constants.INFO_ID),
                        remoteMessage.getData().get(Constants.SHAREHOLDER_NAME),
                        Integer.valueOf(remoteMessage.getData().get(Constants.INFO_PART)),
                        InfoStatus.HOLD,
                        remoteMessage.getData().get(Constants.INFO_SHAREHOLDER_TOKEN));
                shareholder.setCreatedTime(remoteMessage.getSentTime());
                EventBus.getDefault().post(shareholder);
            } else if ((remoteMessage.getData().get(Constants.MESSAGE_TYPE)).equals(Constants.OWNER_APPROVED)) {
                Map<String, Object> args = new HashMap<>();
                args.put(Constants.MESSAGE_TYPE, Constants.OWNER_APPROVED);
                args.put(Constants.INFO_ID, remoteMessage.getData().get(Constants.INFO_ID));
                args.put(Constants.INFO_NAME, remoteMessage.getData().get(Constants.INFO_NAME));
                args.put(Constants.OWNER_NAME, remoteMessage.getData().get(Constants.OWNER_NAME));
                args.put(Constants.INFO_STATUS, InfoStatus.HOLD);
                EventBus.getDefault().post(args);
            } else if ((remoteMessage.getData().get(Constants.MESSAGE_TYPE)).equals(Constants.OWNER_REQUESTED)) {
                showNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
                Map<String, Object> args = new HashMap<>();
                args.put(Constants.MESSAGE_TYPE, Constants.STATUS_CHANGED);
                EventBus.getDefault().post(args);
            } else if ((remoteMessage.getData().get(Constants.MESSAGE_TYPE)).equals(Constants.STATUS_CHANGED)) {
                Map<String, Object> args = new HashMap<>();
                args.put(Constants.MESSAGE_TYPE, Constants.STATUS_CHANGED);
                args.put(Constants.INFO_STATUS, remoteMessage.getData().get(Constants.INFO_STATUS));
                if (remoteMessage.getData().get(Constants.INFO_PART) != null) {
                    args.put(Constants.INFO_PART, Integer.valueOf(remoteMessage.getData().get(Constants.INFO_PART)));
                }
                EventBus.getDefault().post(args);
            }
        }
    }

    private void showNotification(String title, String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.account_icon)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManagerCompat.from(this).notify(notificationId++, builder.build());

    }
}
