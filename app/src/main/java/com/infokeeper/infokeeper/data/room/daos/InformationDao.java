package com.infokeeper.infokeeper.data.room.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.infokeeper.infokeeper.data.models.Information;

import java.util.List;

@Dao
public interface InformationDao {

    @Query("SELECT * FROM information WHERE ownerId LIKE :ownerId")
    LiveData<List<Information>> getInformationsLiveDataByOwner(String ownerId);

    @Query("SELECT * FROM information WHERE ownerId LIKE :ownerId")
    List<Information> getInformationsByOwner(String ownerId);

    @Query("SELECT * FROM information WHERE ownerId IS NULL AND ownerName IS NOT NULL AND ownerName != \"\" ")
    LiveData<List<Information>> getInformationsExceptOwner();

    @Query("SELECT * FROM information WHERE id LIKE :infoId")
    Information getInformationById(String infoId);

    @Query("SELECT * FROM information WHERE id LIKE :infoId AND holdersCount = :part AND ownerName IS NOT NULL AND ownerName != \"\" ")
    Information getInformationByIdAndPart(String infoId,int part);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Information information);

    @Update
    void update(Information information);

    @Query("UPDATE information SET name=:infoName, ownerName=:ownerName,status=:status WHERE id = :infoId")
    void updateOtherInfo(String infoId, String infoName, String ownerName, String status);

    @Query("UPDATE information SET content=:content,holdersCount=:count WHERE id = :infoId")
    void updateInfo(String infoId, String content, int count);

    @Query("UPDATE information SET status=:infoStatus WHERE id =:infoId")
    void updateStatus(String infoId, String infoStatus);

    @Query("UPDATE information SET status=:infoStatus WHERE id =:infoId AND holdersCount =:part")
    void updateOtherInfoStatus(String infoId,int part, String infoStatus);

    @Query("DELETE FROM information WHERE id =:infoId AND holdersCount =:part")
    void deleteOtherInfo(String infoId,int part);

    @Delete
    void delete(Information information);

    @Query("DELETE FROM information")
    void clearTable();
}
