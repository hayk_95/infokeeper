package com.infokeeper.infokeeper.data.room.converters;

import android.arch.persistence.room.TypeConverter;

import com.infokeeper.infokeeper.data.enums.InfoStatus;

public class InfoStatusConverter {

    @TypeConverter
    public static InfoStatus getShareholderStatus(String status) {
        if (status.equals(InfoStatus.NOT_SHARED.getStatusName())) {
            return InfoStatus.NOT_SHARED;
        } else if (status.equals(InfoStatus.HOLD.getStatusName())) {
            return InfoStatus.HOLD;
        } else if (status.equals(InfoStatus.NEED_UPDATE.getStatusName())) {
            return InfoStatus.NEED_UPDATE;
        } else if (status.equals(InfoStatus.REQUESTED.getStatusName())) {
            return InfoStatus.REQUESTED;
        } else if (status.equals(InfoStatus.DELETED.getStatusName())) {
            return InfoStatus.DELETED;
        } else if (status.equals(InfoStatus.DENY.getStatusName())) {
            return InfoStatus.DENY;
        } else if (status.equals(InfoStatus.APPROVED.getStatusName())) {
            return InfoStatus.APPROVED;
        } else {
            throw new IllegalArgumentException("Could not recognize status");
        }
    }

    @TypeConverter
    public static String getInfoStatusString(InfoStatus status) {
        return status.getStatusName();
    }
}
