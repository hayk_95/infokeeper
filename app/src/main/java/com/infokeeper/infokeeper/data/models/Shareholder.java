package com.infokeeper.infokeeper.data.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.infokeeper.infokeeper.data.enums.InfoStatus;
import com.infokeeper.infokeeper.data.room.converters.InfoStatusConverter;

@Entity()
public class Shareholder {
    @NonNull
    @PrimaryKey
    private String id;
    @NonNull
    private String infoId;
    private String name;
    private int part;
    @TypeConverters(InfoStatusConverter.class)
    private InfoStatus status;
    private String content;
    private long createdTime;
    private String token;

    public Shareholder(@NonNull String id, @NonNull String infoId, String name, int part, InfoStatus status, String token) {
        this.id = id;
        this.infoId = infoId;
        this.name = name;
        this.part = part;
        this.status = status;
        this.token = token;
    }

    @Ignore
    public Shareholder(@NonNull String id, @NonNull String infoId, String name, int part, InfoStatus status) {
        this.id = id;
        this.infoId = infoId;
        this.name = name;
        this.part = part;
        this.status = status;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public String getInfoId() {
        return infoId;
    }

    public void setInfoId(@NonNull String infoId) {
        this.infoId = infoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPart() {
        return part;
    }

    public void setPart(int part) {
        this.part = part;
    }

    public InfoStatus getStatus() {
        return status;
    }

    public void setStatus(InfoStatus status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
