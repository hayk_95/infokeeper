package com.infokeeper.infokeeper.data.enums;

import com.infokeeper.infokeeper.R;

public enum InfoStatus {
    NOT_SHARED("Not shared", R.color.red),
    HOLD("Hold", R.color.black),
    NEED_UPDATE("Need update", R.color.blue),
    REQUESTED("Requested", R.color.yellow),
    DELETED("Deleted", R.color.dark_green),
    DENY("Deny", R.color.orange),
    APPROVED("Approved", R.color.green);

    private String status;
    private int colorId;

    InfoStatus(String status, int colorId) {
        this.status = status;
        this.colorId = colorId;
    }

    public static InfoStatus getInfoStatus(String name) {
        if (name.equals(NOT_SHARED.getStatusName())) {
            return NOT_SHARED;
        } else if (name.equals(HOLD.getStatusName())) {
            return HOLD;
        } else if (name.equals(NEED_UPDATE.getStatusName())) {
            return NEED_UPDATE;
        } else if (name.equals(REQUESTED.getStatusName())) {
            return REQUESTED;
        } else if (name.equals(DELETED.getStatusName())) {
            return DELETED;
        } else if (name.equals(DENY.getStatusName())) {
            return DENY;
        } else if (name.equals(APPROVED.getStatusName())) {
            return APPROVED;
        } else {
            return null;
        }
    }

    public String getStatusName() {
        return status;
    }

    public int getStatusColor() {
        return colorId;
    }
}
