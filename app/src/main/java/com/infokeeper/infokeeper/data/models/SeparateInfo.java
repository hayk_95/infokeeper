package com.infokeeper.infokeeper.data.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity()
public class SeparateInfo {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String parentInfoId;
    private int part;
    private String content;

    public SeparateInfo(String parentInfoId, int part, String content) {
        this.parentInfoId = parentInfoId;
        this.part = part;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getParentInfoId() {
        return parentInfoId;
    }

    public void setParentInfoId(String parentInfoId) {
        this.parentInfoId = parentInfoId;
    }

    public int getPart() {
        return part;
    }

    public void setPart(int part) {
        this.part = part;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
