package com.infokeeper.infokeeper.data;

import android.support.annotation.NonNull;

import com.infokeeper.infokeeper.data.enums.Status;

import static com.infokeeper.infokeeper.data.enums.Status.ERROR;
import static com.infokeeper.infokeeper.data.enums.Status.LOADING;
import static com.infokeeper.infokeeper.data.enums.Status.SUCCESS;

public class StatusResource {
    @NonNull
    public Status status;
    public String message;

    private StatusResource(@NonNull Status status, String message) {
        this.status = status;
        this.message = message;
    }

    public static StatusResource success() {
        return new StatusResource(SUCCESS, null);
    }

    public static StatusResource error(String msg) {
        return new StatusResource(ERROR, msg);
    }

    public static StatusResource loading() {
        return new StatusResource(LOADING, null);
    }
}
