package com.infokeeper.infokeeper.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.WriteBatch;
import com.google.firebase.iid.FirebaseInstanceId;
import com.infokeeper.infokeeper.utils.AppExecutors;
import com.infokeeper.infokeeper.Constants;
import com.infokeeper.infokeeper.data.enums.InfoStatus;
import com.infokeeper.infokeeper.data.models.Information;
import com.infokeeper.infokeeper.data.models.QRObject;
import com.infokeeper.infokeeper.data.models.SeparateInfo;
import com.infokeeper.infokeeper.data.models.Shareholder;
import com.infokeeper.infokeeper.data.room.AppDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class DataRepository {

    private static final String TOKEN_PREF_ID = "tokenPrefId";
    public static final int LIVE_DATA_OBJECT = 0;
    public static final int REGISTRATION_LISTENER_OBJECT = 1;

    private static DataRepository instance;
    private FirebaseAuth auth;
    private FirebaseFirestore serverDatabase;
    private AppDatabase localDatabase;
    private AppExecutors executors;
    private SharedPreferences preferences;

    private DataRepository(Context context) {
        auth = FirebaseAuth.getInstance();
        serverDatabase = FirebaseFirestore.getInstance();
        localDatabase = AppDatabase.getInstance(context);
        executors = new AppExecutors();
        preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        if (getToken() == null) {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            String token = task.getResult().getToken();
                            saveToken(token);
                        }
                    });
        }
    }

    public static DataRepository getInstance(Context context) {
        if (instance == null) {
            instance = new DataRepository(context);
        }
        return instance;
    }

    //Server

    public MutableLiveData<StatusResource> createAccount(String name, String email, String password) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                        .setDisplayName(name)
                        .build();
                task.getResult().getUser().updateProfile(profileUpdates).addOnCompleteListener(task1 -> {
                    liveData.postValue(StatusResource.success());
                });
            }
        }).addOnFailureListener(e -> liveData.postValue(StatusResource.error(e.getMessage())));
        return liveData;
    }

    public LiveData<StatusResource> reauthenticate(String password) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());
        AuthCredential credential = EmailAuthProvider.getCredential(auth.getCurrentUser().getEmail(), password);
        auth.getCurrentUser().reauthenticate(credential).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                liveData.postValue(StatusResource.success());
            } else {
                liveData.postValue(StatusResource.error(task.getException().getMessage()));
            }
        });
        return liveData;
    }

    public LiveData<StatusResource> removeAccount(String password) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());
        AuthCredential credential = EmailAuthProvider.getCredential(auth.getCurrentUser().getEmail(), password);
        auth.getCurrentUser().reauthenticate(credential).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                auth.getCurrentUser().delete().addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        liveData.postValue(StatusResource.success());
                    } else {
                        liveData.postValue(StatusResource.error(task1.getException().getMessage()));
                    }
                });
            } else {
                liveData.postValue(StatusResource.error(task.getException().getMessage()));
            }
        });
        return liveData;
    }

    public MutableLiveData<Resource> addInfoExistingRepo(String infoName, String password) {
        Map<String, Object> info = new HashMap<>();
        info.put(Constants.NAME, infoName);
        info.put(Constants.INFO_OWNER, Objects.requireNonNull(auth.getCurrentUser()).getUid());
        info.put(Constants.INFO_STATUS, InfoStatus.NOT_SHARED.getStatusName());
        info.put(Constants.INFO_OWNER_TOKEN, getToken());
        info.put(Constants.INFO_KEY_PASSWORD, password);
        MutableLiveData<Resource> liveData = new MutableLiveData<>();
        liveData.postValue(Resource.loading(null));
        serverDatabase.collection(Constants.INFOS_COLLECTION)
                .add(info)
                .addOnSuccessListener(documentReference -> liveData.postValue(Resource.success(documentReference.getId())))
                .addOnFailureListener(e -> liveData.postValue(Resource.error(e.getMessage(), null)));
        return liveData;
    }

    public ListenerRegistration checkOwnInfosUpdates() {
        return serverDatabase.collection(Constants.INFOS_COLLECTION)
                .whereEqualTo(Constants.INFO_OWNER, auth.getUid())
                .addSnapshotListener(MetadataChanges.INCLUDE, (queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        return;
                    } else if (queryDocumentSnapshots != null) {
                        for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                            if (dc.getType() == DocumentChange.Type.ADDED || dc.getType() == DocumentChange.Type.MODIFIED) {
                                updateInfoStatus(dc.getDocument().getId(), ((String) dc.getDocument().get(Constants.INFO_STATUS)));
                            }
                        }
                    }
                });
    }

    public ListenerRegistration checkOtherInfosUpdates() {
        return serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION)
                .whereEqualTo(Constants.SHAREHOLDER_ID, auth.getUid())
                .addSnapshotListener(MetadataChanges.INCLUDE, (queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        return;
                    } else if (queryDocumentSnapshots != null) {
                        for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                            if (dc.getType() == DocumentChange.Type.ADDED || dc.getType() == DocumentChange.Type.MODIFIED) {
                                if(isInfoExist(((String) dc.getDocument().get(Constants.INFO_ID)),dc.getDocument().getLong(Constants.INFO_PART).intValue())){
                                    updateOtherInfoStatus(((String) dc.getDocument().get(Constants.INFO_ID)),dc.getDocument().getLong(Constants.INFO_PART).intValue(), ((String) dc.getDocument().get(Constants.INFO_STATUS)));
                                }else {
                                    Information information = new Information(((String) dc.getDocument().get(Constants.INFO_ID)),
                                            ((String) dc.getDocument().get(Constants.INFO_NAME)),
                                            ((String) dc.getDocument().get(Constants.OWNER_NAME)),
                                            InfoStatus.getInfoStatus((String) dc.getDocument().get(Constants.INFO_STATUS)));
                                    updateOtherInfo(information);
                                }
                            }else if(dc.getType() == DocumentChange.Type.REMOVED){
                                if(isInfoExist(((String) dc.getDocument().get(Constants.INFO_ID)),dc.getDocument().getLong(Constants.INFO_PART).intValue())){
                                    removeOtherInfo(((String) dc.getDocument().get(Constants.INFO_ID)),dc.getDocument().getLong(Constants.INFO_PART).intValue());
                                }
                            }
                        }
                    }
                });
    }

    public MutableLiveData<StatusResource> saveShareHolder(Shareholder shareholder, String infoName) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());

        Map<String, Object> holderValues = new HashMap<>();
        holderValues.put(Constants.SHAREHOLDER_ID, shareholder.getId());
        holderValues.put(Constants.INFO_ID, shareholder.getInfoId());
        holderValues.put(Constants.INFO_PART, shareholder.getPart());
        holderValues.put(Constants.INFO_STATUS, InfoStatus.HOLD.getStatusName());
        holderValues.put(Constants.SHAREHOLDER_NAME, shareholder.getName());
        holderValues.put(Constants.INFO_NAME, infoName);
        holderValues.put(Constants.OWNER_NAME, auth.getCurrentUser().getDisplayName());
        holderValues.put(Constants.INFO_SHAREHOLDER_TOKEN, shareholder.getToken());
        serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION).document().set(holderValues).addOnSuccessListener(aVoid ->
                liveData.postValue(StatusResource.success())).addOnFailureListener(e -> liveData.postValue(StatusResource.error(e.getMessage())));
        return liveData;
    }

    public Map<Integer, Object> checkShareholdersRequests(String infoId, int infoPart) {
        MutableLiveData<Resource<Shareholder>> liveData = new MutableLiveData<>();
        Map<Integer, Object> list = new HashMap<>();
        liveData.postValue(Resource.loading(null));

        ListenerRegistration registration = serverDatabase.collection(Constants.SHAREHOLDERS_REQUEST_COLLECTION)
                .whereEqualTo(Constants.INFO_ID, infoId).whereEqualTo(Constants.INFO_PART, infoPart)
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        liveData.postValue(Resource.error(e.getMessage(), null));
                        return;
                    }
                    if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()) {
                        for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                            if (dc.getType() == DocumentChange.Type.ADDED) {
                                liveData.postValue(Resource.success(new Shareholder(((String) dc.getDocument().get(Constants.SHAREHOLDER_ID)),
                                        ((String) dc.getDocument().get(Constants.INFO_ID)),
                                        ((String) dc.getDocument().get(Constants.SHAREHOLDER_NAME)),
                                        dc.getDocument().getLong(Constants.INFO_PART).intValue(),
                                        InfoStatus.HOLD,
                                        ((String) dc.getDocument().get(Constants.INFO_SHAREHOLDER_TOKEN)))));
                            }
                        }
                    }

                });

        list.put(LIVE_DATA_OBJECT, liveData);
        list.put(REGISTRATION_LISTENER_OBJECT, registration);
        return list;
    }

    public MutableLiveData<StatusResource> sendShareHolderRequest(QRObject qrObject) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());

        Map<String, Object> shareHolder = new HashMap<>();
        shareHolder.put(Constants.SHAREHOLDER_ID, auth.getCurrentUser().getUid());
        shareHolder.put(Constants.INFO_ID, qrObject.getInfoId());
        shareHolder.put(Constants.INFO_PART, qrObject.getInfoPart());
        shareHolder.put(Constants.SHAREHOLDER_NAME, auth.getCurrentUser().getDisplayName());
        shareHolder.put(Constants.INFO_SHAREHOLDER_TOKEN, getToken());
        serverDatabase.collection(Constants.INFOS_COLLECTION).document(qrObject.getInfoId()).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                shareHolder.put(Constants.REGISTRATION_TOKEN, task.getResult().get(Constants.INFO_OWNER_TOKEN));
                serverDatabase.collection(Constants.SHAREHOLDERS_REQUEST_COLLECTION).document().set(shareHolder).addOnSuccessListener(aVoid -> {
                    saveOtherInfo(qrObject.getInfoId(), qrObject.getInfoContent(),qrObject.getInfoPart());
                    liveData.postValue(StatusResource.success());
                }).addOnFailureListener(e -> liveData.postValue(StatusResource.error(e.getMessage())));
            }
        });
        return liveData;
    }

    public void removeShareholderRequests(String infoId) {
        serverDatabase.collection(Constants.SHAREHOLDERS_REQUEST_COLLECTION).whereEqualTo(Constants.INFO_ID, infoId).get().addOnCompleteListener(task -> {
            if (task.isSuccessful() && task.getResult() != null) {
                WriteBatch batch = serverDatabase.batch();
                for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                    batch.delete(documentSnapshot.getReference());
                }
                batch.commit();
            }
        });
    }

    public MutableLiveData<StatusResource> requestInfosParts(Information information) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());

        serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION)
                .whereEqualTo(Constants.INFO_ID, information.getId())
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                WriteBatch batch = serverDatabase.batch();
                List<String> list = new ArrayList<>();
                for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                    batch.update(documentSnapshot.getReference(), Constants.INFO_STATUS, InfoStatus.REQUESTED.getStatusName());
                    list.add(((String) documentSnapshot.get(Constants.INFO_SHAREHOLDER_TOKEN)));
                }
                Map<String, Object> infoRequest = new HashMap<>();
                infoRequest.put(Constants.REGISTRATION_TOKEN, list);
                infoRequest.put(Constants.INFO_ID, information.getId());
                infoRequest.put(Constants.INFO_NAME, information.getName());
                infoRequest.put(Constants.OWNER_NAME, information.getOwnerName());

                batch.commit().addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        serverDatabase.collection(Constants.INFO_REQUEST_COLLECTION).add(infoRequest).addOnCompleteListener(task2 -> {
                            if (task2.isSuccessful()) {
                                liveData.postValue(StatusResource.success());
                                serverDatabase.collection(Constants.INFOS_COLLECTION).document(information.getId())
                                        .update(Constants.INFO_STATUS, InfoStatus.REQUESTED.getStatusName())
                                        .addOnCompleteListener(task3 -> {
                                            if (task3.isSuccessful()) {
                                                information.setStatus(InfoStatus.REQUESTED);
                                                information.setRequested(true);
                                                executors.diskIO().execute(() -> localDatabase.informationDao().update(information));
                                            }
                                        });
                            } else {
                                liveData.postValue(StatusResource.error(task2.getException().getMessage()));
                            }
                        });
                    }
                });
            }
        });
        return liveData;
    }

    public void updateRegistrationToken(String token) {
        if (token != null && auth.getUid() != null) {
            serverDatabase.collection(Constants.INFOS_COLLECTION).whereEqualTo(Constants.INFO_OWNER, auth.getUid()).get().addOnCompleteListener(task -> {
                if (task.isSuccessful() && task.getResult() != null && !task.getResult().getDocuments().isEmpty()) {
                    WriteBatch batch = serverDatabase.batch();
                    for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                        batch.update(documentSnapshot.getReference(), Constants.INFO_OWNER_TOKEN, token);
                    }
                    batch.commit();
                }
            });
            serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION).whereEqualTo(Constants.SHAREHOLDER_ID, auth.getUid()).get().addOnCompleteListener(task -> {
                if (task.isSuccessful() && task.getResult() != null && !task.getResult().getDocuments().isEmpty()) {
                    WriteBatch batch = serverDatabase.batch();
                    for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                        batch.update(documentSnapshot.getReference(), Constants.INFO_SHAREHOLDER_TOKEN, token);
                    }
                    batch.commit();
                }
            });
        }
    }

    public MutableLiveData<StatusResource> sendInfoPart(Information information) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());

        serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION).whereEqualTo(Constants.INFO_ID, information.getId())
                .whereEqualTo(Constants.SHAREHOLDER_ID, auth.getUid())
                .whereEqualTo(Constants.INFO_PART, information.getHoldersCount())
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                task.getResult().getDocuments().get(0).getReference()
                        .update(Constants.INFO_CONTENT, information.getContent(), Constants.INFO_STATUS, information.getStatus().getStatusName())
                        .addOnCompleteListener(task1 -> {
                            if (task1.isSuccessful()) {
                                executors.diskIO().execute(() -> localDatabase.informationDao().delete(information));
                                liveData.postValue(StatusResource.success());
                            } else {
                                liveData.postValue(StatusResource.error(task1.getException().getMessage()));
                            }
                        });
            } else {
                liveData.postValue(StatusResource.error(task.getException().getMessage()));
            }
        });

        return liveData;
    }

    public MutableLiveData<StatusResource> denyRequest(Information information) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());

        serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION).whereEqualTo(Constants.INFO_ID, information.getId())
                .whereEqualTo(Constants.SHAREHOLDER_ID, auth.getUid())
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION).document(task.getResult().getDocuments().get(0).getId())
                        .update(Constants.INFO_STATUS, InfoStatus.DENY.getStatusName())
                        .addOnCompleteListener(task1 -> {
                            if (task1.isSuccessful()) {
                                updateOtherInfoStatus(information.getId(),information.getHoldersCount(), InfoStatus.DENY.getStatusName());
                                liveData.postValue(StatusResource.success());
                            } else {
                                liveData.postValue(StatusResource.error(task1.getException().getMessage()));
                            }
                        });
            } else {
                liveData.postValue(StatusResource.error(task.getException().getMessage()));
            }
        });

        return liveData;
    }

    public Map<Integer, Object> getShareholders(String infoId) {
        MutableLiveData<Resource<ArrayList<Shareholder>>> liveData = new MutableLiveData<>();
        Map<Integer, Object> list = new HashMap<>();
        liveData.postValue(Resource.loading(null));

        ListenerRegistration registration =serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION)
                .whereEqualTo(Constants.INFO_ID, infoId)
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        liveData.postValue(Resource.error(e.getMessage(), null));
                        return;
                    }
                    if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()) {
                        ArrayList<Shareholder> holders = new ArrayList<>();
                        for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                            if (dc.getType() == DocumentChange.Type.ADDED || dc.getType() == DocumentChange.Type.MODIFIED) {
                                holders.add(new Shareholder(dc.getDocument().getId(),
                                        ((String) dc.getDocument().get(Constants.INFO_ID)),
                                        ((String) dc.getDocument().get(Constants.SHAREHOLDER_NAME)),
                                        dc.getDocument().getLong(Constants.INFO_PART).intValue(),
                                        (InfoStatus.getInfoStatus((String) dc.getDocument().get(Constants.INFO_STATUS))),
                                        ((String) dc.getDocument().get(Constants.INFO_SHAREHOLDER_TOKEN))));
                            }else {
                                ArrayList<Shareholder> deletedHolder = new ArrayList<>();
                                deletedHolder.add(new Shareholder(dc.getDocument().getId(),
                                        ((String) dc.getDocument().get(Constants.INFO_ID)),
                                        ((String) dc.getDocument().get(Constants.SHAREHOLDER_NAME)),
                                        dc.getDocument().getLong(Constants.INFO_PART).intValue(),
                                        (InfoStatus.getInfoStatus((String) dc.getDocument().get(Constants.INFO_STATUS))),
                                        ((String) dc.getDocument().get(Constants.INFO_SHAREHOLDER_TOKEN))));
                                liveData.postValue(Resource.error(null,deletedHolder));
                            }
                        }
                        liveData.postValue(Resource.success(holders));
                    }else {
                        liveData.postValue(Resource.success(null));
                    }
        });

        list.put(LIVE_DATA_OBJECT, liveData);
        list.put(REGISTRATION_LISTENER_OBJECT, registration);
        return list;
    }

    public MutableLiveData<StatusResource> sendRequestToSingleHolder(Shareholder shareholder) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());

        List<String> list = new ArrayList<>();
        list.add(shareholder.getToken());
        Map<String, Object> infoRequest = new HashMap<>();
        infoRequest.put(Constants.REGISTRATION_TOKEN, list);
        infoRequest.put(Constants.INFO_ID, shareholder.getInfoId());
        infoRequest.put(Constants.INFO_NAME, shareholder.getName());
        infoRequest.put(Constants.OWNER_NAME, auth.getCurrentUser().getDisplayName());

        serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION).document(shareholder.getId())
                .update(Constants.INFO_STATUS, InfoStatus.REQUESTED.getStatusName())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        serverDatabase.collection(Constants.INFO_REQUEST_COLLECTION).add(infoRequest);
                        liveData.postValue(StatusResource.success());
                    } else {
                        liveData.postValue(StatusResource.error(task.getException().getMessage()));
                    }
                });

        return liveData;
    }

    public MutableLiveData<StatusResource> getInfoPart(Shareholder shareholder) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());

        serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION).document(shareholder.getId()).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                saveSeparateInfo(new SeparateInfo(shareholder.getInfoId(), shareholder.getPart(), ((String) task.getResult().get(Constants.INFO_CONTENT))));
                deleteShareHolders(shareholder.getInfoId(),shareholder.getPart(), liveData);
            } else {
                liveData.postValue(StatusResource.error(task.getException().getMessage()));
            }
        });

        return liveData;
    }

    public MutableLiveData<StatusResource> deleteShareHolder(String holderId, MutableLiveData<StatusResource> liveData) {
        serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION).document(holderId).delete().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                liveData.postValue(StatusResource.success());
            } else {
                liveData.postValue(StatusResource.error(task.getException().getMessage()));
            }
        });

        return liveData;
    }

    public MutableLiveData<StatusResource> deleteShareHolders(String infoId, int part, MutableLiveData<StatusResource> liveData) {
        serverDatabase.collection(Constants.SHAREHOLDERS_COLLECTION)
                .whereEqualTo(Constants.INFO_ID, infoId)
                .whereEqualTo(Constants.INFO_PART, part)
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful() && task.getResult() != null) {
                WriteBatch batch = serverDatabase.batch();
                for (DocumentSnapshot documentSnapshot:task.getResult().getDocuments()){
                    batch.delete(documentSnapshot.getReference());
                }
                batch.commit();
                liveData.postValue(StatusResource.success());
            } else {
                liveData.postValue(StatusResource.error(task.getException().getMessage()));
            }
        });

        return liveData;
    }

    public LiveData<StatusResource> deleteInfo(String infoId) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());

        serverDatabase.collection(Constants.INFOS_COLLECTION).document(infoId).delete().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                liveData.postValue(StatusResource.success());
            } else {
                liveData.postValue(StatusResource.error(task.getException().getMessage()));
            }
        });

        return liveData;
    }

    public MutableLiveData<Resource<String>> getInfoKeyPassword(String infoId) {
        MutableLiveData<Resource<String>> liveData = new MutableLiveData<>();
        liveData.postValue(Resource.loading(null));

        serverDatabase.collection(Constants.INFOS_COLLECTION).document(infoId).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                liveData.postValue(Resource.success(((String) task.getResult().get(Constants.INFO_KEY_PASSWORD))));
            } else {
                liveData.postValue(Resource.error(task.getException().getMessage(), null));
            }
        });

        return liveData;
    }

    public LiveData<StatusResource> updateInfoStatus(String infoId, InfoStatus status) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        liveData.postValue(StatusResource.loading());

        serverDatabase.collection(Constants.INFOS_COLLECTION).document(infoId).update(Constants.INFO_STATUS, status.getStatusName()).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                updateInfoStatus(infoId, status.getStatusName());
                liveData.postValue(StatusResource.success());
            } else {
                liveData.postValue(StatusResource.error(task.getException().getMessage()));
            }
        });

        return liveData;
    }

    //Database

    public void saveOwnInfo(String infoId, String infoName, String infoContent) {
        executors.diskIO().execute(() -> localDatabase.informationDao().insert(new Information(infoId, auth.getUid(), infoName, auth.getCurrentUser().getDisplayName(), infoContent)));
    }

    public void updateOwnInfo(String infoId, String infoContent, int holderCount) {
        executors.diskIO().execute(() -> localDatabase.informationDao().updateInfo(infoId, infoContent, holderCount));
    }

    public void saveOtherInfo(String infoId, String infoContent, int part) {
        executors.diskIO().execute(() -> localDatabase.informationDao().insert(new Information(infoId, infoContent,part)));
    }

    public void updateOtherInfo(Information information) {
        executors.diskIO().execute(() -> localDatabase.informationDao().updateOtherInfo(information.getId(), information.getName(), information.getOwnerName(), information.getStatus().getStatusName()));
    }

    public LiveData<List<Information>> getMyInformation() {
        return localDatabase.informationDao().getInformationsLiveDataByOwner(auth.getUid());
    }

    public LiveData<List<Information>> getOtherInformation() {
        return localDatabase.informationDao().getInformationsExceptOwner();
    }

    public Information getInformationById(String infoId) {
        return localDatabase.informationDao().getInformationById(infoId);
    }

    public void saveSeparateInfo(SeparateInfo info) {
        executors.diskIO().execute(() -> localDatabase.separateInfoDao().insert(info));
    }

    public List<SeparateInfo> getSeparateInfoParts(String parentInfoId) {
        return localDatabase.separateInfoDao().getSeparateInfosByParentId(parentInfoId);
    }

    public void updateInfoStatus(String infoId, String infoStatus) {
        executors.diskIO().execute(() -> localDatabase.informationDao().updateStatus(infoId, infoStatus));
    }

    public void updateOtherInfoStatus(String infoId,int part, String infoStatus) {
        executors.diskIO().execute(() -> localDatabase.informationDao().updateOtherInfoStatus(infoId,part, infoStatus));
    }

    private boolean isInfoExist(String infoId, int part){
        return localDatabase.informationDao().getInformationByIdAndPart(infoId,part) != null;
    }

    private void removeOtherInfo(String infoId,int part){
        localDatabase.informationDao().deleteOtherInfo(infoId,part);
    }

    public void removeAllData() {
        localDatabase.informationDao().clearTable();
        localDatabase.separateInfoDao().clearTable();
    }

    //Shared Preferences

    public void saveToken(String token) {
        preferences.edit().putString(TOKEN_PREF_ID, token).apply();
    }

    public String getToken() {
        return preferences.getString(TOKEN_PREF_ID, null);
    }
}
