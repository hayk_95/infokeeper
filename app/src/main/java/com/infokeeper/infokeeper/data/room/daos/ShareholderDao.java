package com.infokeeper.infokeeper.data.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.infokeeper.infokeeper.data.models.Shareholder;

import java.util.List;

@Dao
public interface ShareholderDao {
    @Query("SELECT * FROM shareholder WHERE infoId LIKE :infoId")
    List<Shareholder> getShareholdersByInfo(String infoId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Shareholder shareholder);

    @Update
    void update(Shareholder shareholder);

    @Query("UPDATE shareholder SET status=:holderStatus WHERE id = :holderId")
    void updateStatus(String holderId, int holderStatus);

    @Delete
    void delete(Shareholder shareholder);
}
