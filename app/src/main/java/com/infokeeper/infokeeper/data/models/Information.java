package com.infokeeper.infokeeper.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.infokeeper.infokeeper.data.enums.InfoStatus;
import com.infokeeper.infokeeper.data.room.converters.InfoStatusConverter;

@Entity
public class Information {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    private long initialId;
    @NonNull
    private String id;
    private String ownerId;
    private String name;
    private String ownerName;
    private String content;
    @TypeConverters(InfoStatusConverter.class)
    private InfoStatus status = InfoStatus.NOT_SHARED;
    private int holdersCount = 0;
    private boolean isRequested = false;

    public Information(@NonNull String id, String ownerId, String name, String ownerName, String content) {
        this.id = id;
        this.ownerId = ownerId;
        this.name = name;
        this.ownerName = ownerName;
        this.content = content;
    }

    @Ignore
    public Information(@NonNull String id, String content, int holdersCount) {
        this.id = id;
        this.content = content;
        this.holdersCount = holdersCount;
    }

    @Ignore
    public Information(@NonNull String id, String name, String ownerName, InfoStatus status) {
        this.id = id;
        this.name = name;
        this.ownerName = ownerName;
        this.status = status;
    }
    @Ignore
    public Information(@NonNull String id, String ownerId, String name, String ownerName, InfoStatus status) {
        this.id = id;
        this.ownerId = ownerId;
        this.name = name;
        this.ownerName = ownerName;
        this.status = status;
    }

    public long getInitialId() {
        return initialId;
    }

    public void setInitialId(long initialId) {
        this.initialId = initialId;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public InfoStatus getStatus() {
        return status;
    }

    public void setStatus(InfoStatus status) {
        this.status = status;
    }

    public int getHoldersCount() {
        return holdersCount;
    }

    public void setHoldersCount(int holderCount) {
        this.holdersCount = holderCount;
    }

    public boolean isRequested() {
        return isRequested;
    }

    public void setRequested(boolean requested) {
        isRequested = requested;
    }
}
