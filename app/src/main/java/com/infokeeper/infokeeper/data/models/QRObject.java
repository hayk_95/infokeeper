package com.infokeeper.infokeeper.data.models;

import org.json.JSONException;
import org.json.JSONObject;

public class QRObject {

    private static final String INFO_ID = "info_id";
    private static final String INFO_CONTENT = "info_content";
    private static final String INFO_PART = "info_part";

    private String infoId;
    private String infoContent;
    private int infoPart;

    public QRObject(String infoId, String infoContent, int infoPart) {
        this.infoId = infoId;
        this.infoContent = infoContent;
        this.infoPart = infoPart;
    }

    public String getInfoId() {
        return infoId;
    }

    public void setInfoId(String infoId) {
        this.infoId = infoId;
    }

    public String getInfoContent() {
        return infoContent;
    }

    public void setInfoContent(String infoContent) {
        this.infoContent = infoContent;
    }

    public int getInfoPart() {
        return infoPart;
    }

    public void setInfoPart(int infoPart) {
        this.infoPart = infoPart;
    }

    public static QRObject parse(JSONObject jsonObject) {
        return new QRObject(jsonObject.optString(INFO_ID),
                jsonObject.optString(INFO_CONTENT),
                jsonObject.optInt(INFO_PART, -1));
    }

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(INFO_ID, infoId)
                    .put(INFO_CONTENT, infoContent)
                    .put(INFO_PART, infoPart);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}
