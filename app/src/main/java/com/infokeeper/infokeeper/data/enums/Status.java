package com.infokeeper.infokeeper.data.enums;

public enum Status {
    SUCCESS,
    LOADING,
    ERROR
}
