package com.infokeeper.infokeeper.data.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.infokeeper.infokeeper.data.models.SeparateInfo;
import com.infokeeper.infokeeper.data.models.Shareholder;
import com.infokeeper.infokeeper.data.room.daos.InformationDao;
import com.infokeeper.infokeeper.data.models.Information;
import com.infokeeper.infokeeper.data.room.daos.SeparateInfoDao;
import com.infokeeper.infokeeper.data.room.daos.ShareholderDao;

@Database(entities = {Information.class, Shareholder.class, SeparateInfo.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;
    private static final String DATABASE_NAME = "infokeeper-db";

    public static AppDatabase getInstance(final Context context) {
        if (instance == null) {
            synchronized (AppDatabase.class) {
                if (instance == null) {
                    instance = buildDatabase(context.getApplicationContext());
                }
            }
        }
        return instance;
    }

    private static AppDatabase buildDatabase(final Context appContext) {
        return Room.databaseBuilder(appContext, AppDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
    }

    public abstract InformationDao informationDao();

    public abstract ShareholderDao shareholderDao();

    public abstract SeparateInfoDao separateInfoDao();
}
