package com.infokeeper.infokeeper.data.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.infokeeper.infokeeper.data.models.SeparateInfo;

import java.util.List;

@Dao
public interface SeparateInfoDao {

    @Query("SELECT * FROM separateinfo WHERE parentInfoId LIKE :parentId")
    List<SeparateInfo> getSeparateInfosByParentId(String parentId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SeparateInfo info);

    @Update
    void update(SeparateInfo info);

    @Delete
    void delete(SeparateInfo info);

    @Query("DELETE FROM separateinfo")
    void clearTable();
}
