package com.infokeeper.infokeeper.data.models;

public class XORSplitObject {
    private byte[] ownPart;
    private byte[] holderPart;

    public XORSplitObject(byte[] ownPart, byte[] holderPart) {
        this.ownPart = ownPart;
        this.holderPart = holderPart;
    }

    public byte[] getOwnPart() {
        return ownPart;
    }

    public void setOwnPart(byte[] ownPart) {
        this.ownPart = ownPart;
    }

    public byte[] getHolderPart() {
        return holderPart;
    }

    public void setHolderPart(byte[] holderPart) {
        this.holderPart = holderPart;
    }
}
