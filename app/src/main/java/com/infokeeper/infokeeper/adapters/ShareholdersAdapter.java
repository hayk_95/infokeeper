package com.infokeeper.infokeeper.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.data.enums.InfoStatus;
import com.infokeeper.infokeeper.data.models.Shareholder;

import java.util.ArrayList;
import java.util.List;

public class ShareholdersAdapter extends RecyclerView.Adapter<ShareholdersAdapter.ShareholderHolder> {

    private ArrayList<Shareholder> shareholders;
    private OnShareholderItemEventListener itemEventListener;
    private boolean isRequested;

    public ShareholdersAdapter(boolean isRequested) {
        shareholders = new ArrayList<>();
        this.isRequested = isRequested;
    }

    @NonNull
    @Override
    public ShareholderHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ShareholderHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.shareholder_holder_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ShareholderHolder shareholderHolder, int i) {
        shareholderHolder.name.setText(shareholders.get(i).getName());
        Spannable span = new SpannableString(shareholderHolder.itemView.getContext().getString(R.string.status) + " " + shareholders.get(i).getStatus().getStatusName());
        span.setSpan(new ForegroundColorSpan(shareholderHolder.itemView.getContext().getResources().getColor(shareholders.get(i).getStatus().getStatusColor())),
                shareholderHolder.itemView.getContext().getString(R.string.status).length(), span.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        shareholderHolder.status.setText(span);
        shareholderHolder.part.setText(String.valueOf(shareholders.get(i).getPart()));
        shareholderHolder.itemView.setOnClickListener(v -> {
            if (itemEventListener != null) {
                itemEventListener.onShareholderClicked(shareholders.get(i));
            }
        });
    }

    public interface OnShareholderItemEventListener {
        void onShareholderClicked(Shareholder shareholder);
    }

    public void setOnShareholderItemEventListener(OnShareholderItemEventListener itemEventListener) {
        this.itemEventListener = itemEventListener;
    }

    @Override
    public int getItemCount() {
        return shareholders.size();
    }

    public void updateShareholders(List<Shareholder> list) {
        if(list != null) {
            for (Shareholder shareholder : list) {
                boolean isExist = false;
                for (Shareholder shareholder1 : shareholders) {
                    if (shareholder1.getId().equals(shareholder.getId()) && shareholder1.getPart() == shareholder.getPart()) {
                        shareholders.set(shareholders.indexOf(shareholder1), shareholder);
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    shareholders.add(shareholder);
                }
            }
            notifyDataSetChanged();
        }
    }

    public void deleteShareHolder(Shareholder shareholder){
        for (Shareholder shareholder1 : shareholders){
            if(shareholder.getId().equals(shareholder1.getId()) && shareholder.getPart() == shareholder1.getPart()){
                shareholders.remove(shareholder1);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void removeShareholder(Shareholder shareholder) {
        List<Shareholder> list = new ArrayList<>();
        for (Shareholder shareholder1: shareholders){
            if(shareholder1.getPart() == shareholder.getPart()){
                list.add(shareholder1);
            }
        }
        shareholders.removeAll(list);
        notifyDataSetChanged();
    }

    public void updateShareHolderStatus(int part, String status) {
        for (Shareholder shareholder : shareholders) {
            if (shareholder.getPart() == part) {
                shareholder.setStatus(InfoStatus.getInfoStatus(status));
                notifyItemChanged(shareholders.indexOf(shareholder));
                break;
            }
        }
    }

    public InfoStatus getCurrentInfoStatus() {
        InfoStatus status = getItemCount() == 0 ?
                isRequested ? InfoStatus.APPROVED : InfoStatus.NOT_SHARED :
                isRequested ? InfoStatus.REQUESTED : InfoStatus.HOLD;
        for (Shareholder shareholder : shareholders) {
            if (shareholder.getStatus() == InfoStatus.APPROVED || shareholder.getStatus() == InfoStatus.DELETED) {
                status = InfoStatus.NEED_UPDATE;
                break;
            }
        }
        return status;
    }

    class ShareholderHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView status;
        TextView part;

        ShareholderHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.holder_name);
            status = itemView.findViewById(R.id.holder_status);
            part = itemView.findViewById(R.id.info_part);
        }
    }
}
