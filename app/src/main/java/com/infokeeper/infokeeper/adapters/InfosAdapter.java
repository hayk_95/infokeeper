package com.infokeeper.infokeeper.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.data.models.Information;

import java.util.ArrayList;
import java.util.List;

public class InfosAdapter extends RecyclerView.Adapter<InfosAdapter.InfosHolder> {

    private ArrayList<Information> information;
    private boolean isOwnInfos;
    private OnInformationItemEventListener itemEventListener;

    public InfosAdapter(boolean isOwnInfos) {
        information = new ArrayList<>();
        this.isOwnInfos = isOwnInfos;
    }

    @NonNull
    @Override
    public InfosHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new InfosHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.info_holder_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull InfosHolder infosHolder, int i) {
        infosHolder.name.setText(information.get(i).getName());
        Spannable span = new SpannableString(infosHolder.itemView.getContext().getString(R.string.status) + " " + information.get(i).getStatus().getStatusName());
        span.setSpan(new ForegroundColorSpan(infosHolder.itemView.getContext().getResources().getColor(information.get(i).getStatus().getStatusColor())),
                infosHolder.itemView.getContext().getString(R.string.status).length(), span.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        infosHolder.status.setText(span);
        infosHolder.ownerLayout.setVisibility(isOwnInfos ? View.GONE : View.VISIBLE);
        infosHolder.owner.setText(information.get(i).getOwnerName());
        infosHolder.more.setVisibility((!isOwnInfos || information.get(i).isRequested()) ? View.GONE : View.VISIBLE);

        infosHolder.itemView.setOnClickListener(v -> {
            if (itemEventListener != null) {
                itemEventListener.onInformationClicked(information.get(i));
            }
        });

        infosHolder.more.setOnClickListener(v -> {
            if (itemEventListener != null) {
                itemEventListener.onInformationMoreClicked(information.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return information.size();
    }

    public void updateInformation(List<Information> list) {
        information.clear();
        information.addAll(list);
        notifyDataSetChanged();
    }

    public interface OnInformationItemEventListener {
        void onInformationClicked(Information information);

        void onInformationMoreClicked(Information information);
    }

    public void setOnInformationItemEventListener(OnInformationItemEventListener itemEventListener) {
        this.itemEventListener = itemEventListener;
    }

    class InfosHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView owner;
        TextView status;
        ImageView more;
        LinearLayout ownerLayout;

        InfosHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.info_name);
            owner = itemView.findViewById(R.id.owner_name);
            status = itemView.findViewById(R.id.info_status);
            more = itemView.findViewById(R.id.more_info);
            ownerLayout = itemView.findViewById(R.id.owner_layout);
        }
    }
}
