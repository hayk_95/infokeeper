package com.infokeeper.infokeeper;

public class Constants {
    //collection names
    public static final String INFOS_COLLECTION = "infos";
    public static final String SHAREHOLDERS_COLLECTION = "shareholders";
    public static final String INFO_REQUEST_COLLECTION = "infoRequest";
    public static final String SHAREHOLDERS_REQUEST_COLLECTION = "holdersRequest";

    //rows name
    public static final String NAME = "name";
    public static final String INFO_OWNER = "owner";
    public static final String INFO_PART = "part";
    public static final String INFO_STATUS = "status";
    public static final String INFO_ID = "infoId";
    public static final String INFO_CONTENT = "content";
    public static final String SHAREHOLDER_ID = "holderId";
    public static final String SHAREHOLDER_NAME = "holderName";
    public static final String INFO_NAME = "infoName";
    public static final String OWNER_NAME = "ownerName";
    public static final String INFO_OWNER_TOKEN = "ownerToken";
    public static final String INFO_KEY_PASSWORD = "keyPassword";
    public static final String INFO_SHAREHOLDER_TOKEN = "holderToken";
    public static final String REGISTRATION_TOKEN = "token";

    //messages type
    public static final String MESSAGE_TYPE = "type";
    public static final String SHAREHOLDER_ADDED = "holder_added";
    public static final String OWNER_APPROVED = "owner_approved";
    public static final String OWNER_REQUESTED = "owner_requested";
    public static final String STATUS_CHANGED = "status_changed";
}
