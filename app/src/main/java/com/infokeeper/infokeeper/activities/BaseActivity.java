package com.infokeeper.infokeeper.activities;

import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.utils.Utils;
import com.infokeeper.infokeeper.fragments.dialogs.LoadingDialogFragment;

import java.util.Objects;

public class BaseActivity extends AppCompatActivity {
    public static final String LOADING_TAG = "loading";

    private LoadingDialogFragment loadingDialog;

    private int pendingLoadingCount = 0;


    public void showLoadingDialog() {
        if (pendingLoadingCount == 0) {
            dismissCurrentDialog();
            loadingDialog = LoadingDialogFragment.newInstance();
            FragmentManager manager = Objects.requireNonNull(getSupportFragmentManager());
            loadingDialog.show(manager, LOADING_TAG);
        }
        pendingLoadingCount++;
    }

    public void dismissCurrentDialog() {
        if (Objects.requireNonNull(getSupportFragmentManager()).findFragmentByTag(LOADING_TAG) != null) {
            if (pendingLoadingCount > 0) {
                pendingLoadingCount--;
            }
            if (pendingLoadingCount == 0) {
                loadingDialog.dismiss();
            }
        }
    }

    public void showErrorDialog(String errorMsg) {
        dismissCurrentDialog();
        String errorMessage = Utils.isNetworkConnected(this)
                ? (errorMsg != null && !errorMsg.isEmpty()) ? errorMsg : getString(R.string.default_error_message)
                : getString(R.string.no_connection_error_message);
        new AlertDialog.Builder(this)
                .setTitle(R.string.error)
                .setMessage(errorMessage)
                .setPositiveButton(getString(R.string.ok), (dialog, which) -> dialog.dismiss())
                .create().show();
    }

    public void showEditDialog(String title, String message, OnDialogEventListener eventListener) {
        dismissCurrentDialog();
        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        input.setLayoutParams(lp);

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setView(input)
                .setNegativeButton(R.string.cancel,
                        (dialog, which) -> dialog.dismiss())
                .setPositiveButton(R.string.yes,
                        (dialog, which) -> {
                            String text = input.getText().toString();
                            if (eventListener != null) {
                                eventListener.onEditDialogPositiveClick(dialog, input.getText().toString());
                            }
                        }).create().show();
    }

    public interface OnDialogEventListener {
        void onEditDialogPositiveClick(DialogInterface dialog, String text);
    }
}
