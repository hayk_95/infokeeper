package com.infokeeper.infokeeper.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.infokeeper.infokeeper.Constants;
import com.infokeeper.infokeeper.utils.QRUtils;
import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.data.models.QRObject;
import com.infokeeper.infokeeper.fragments.dialogs.LoadingDialogFragment;
import com.infokeeper.infokeeper.viewmodels.QRScannerViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class QRScannerActivity extends AppCompatActivity {
    public static final String LOADING_TAG = "loading";

    private IntentIntegrator qrScan;
    private QRScannerViewModel model;
    private LoadingDialogFragment loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscanner);
        model = ViewModelProviders.of(this).get(QRScannerViewModel.class);
        qrScan = new IntentIntegrator(this);
        qrScan.setDesiredBarcodeFormats(Collections.singleton("QR_CODE"));
        qrScan.setPrompt("");
        qrScan.initiateScan();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void addShareHolder(QRObject qrObject) {
        model.addShareHolder(qrObject).observe(this, statusResource -> {
            if (statusResource != null) {
                switch (statusResource.status) {
                    case SUCCESS:
                        dismissCurrentDialog();
                        Toast.makeText(this, "Shareholder request send success", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case ERROR:
                        dismissCurrentDialog();
                        new AlertDialog.Builder(this)
                                .setTitle(R.string.error)
                                .setMessage(statusResource.message != null ? statusResource.message : getString(R.string.default_error_message))
                                .setPositiveButton(R.string.ok, (dialog, which) -> {
                                    dialog.dismiss();
                                    QRScannerActivity.this.finish();
                                }).create().show();
                        break;
                    case LOADING:
                        showLoadingDialog();
                        break;
                }
            }
        });
    }

    public void showLoadingDialog() {
        dismissCurrentDialog();
        loadingDialog = LoadingDialogFragment.newInstance();
        FragmentManager manager = Objects.requireNonNull(getSupportFragmentManager());
        loadingDialog.show(manager, LOADING_TAG);
    }

    public void dismissCurrentDialog() {
        if (Objects.requireNonNull(getSupportFragmentManager()).findFragmentByTag(LOADING_TAG) != null) {
            loadingDialog.dismiss();
        }
    }

    @Subscribe
    public void onEvent(Map<String, Object> event) {
        runOnUiThread(() -> {
            if (event.get(Constants.MESSAGE_TYPE).equals(Constants.OWNER_APPROVED)) {
                dismissCurrentDialog();
                model.updateOtherInfo(event);
                new AlertDialog.Builder(this)
                        .setTitle(R.string.success)
                        .setMessage(R.string.added_as_holder)
                        .setPositiveButton(R.string.ok, (dialog, which) -> {
                            dialog.dismiss();
                            QRScannerActivity.this.finish();
                        }).create().show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        QRUtils.scan(requestCode, resultCode, data, new QRUtils.QRScanListener() {
            @Override
            public void onScanSuccess(QRObject qRObject) {
                addShareHolder(qRObject);
            }

            @Override
            public void onScanFail() {
                Toast.makeText(QRScannerActivity.this, getString(R.string.qr_not_found), Toast.LENGTH_SHORT).show();
                QRScannerActivity.super.onActivityResult(requestCode, resultCode, data);
                onBackPressed();
            }
        });
    }
}
