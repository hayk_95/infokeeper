package com.infokeeper.infokeeper.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.infokeeper.infokeeper.Constants;
import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.fragments.InfosFragment;
import com.infokeeper.infokeeper.viewmodels.InfosViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Map;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private FirebaseAuth auth;
    private NavigationView navigationView;

    private InfosViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, 0, 0);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        model = ViewModelProviders.of(this).get(InfosViewModel.class);
        checkUserExist();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void checkUserExist() {
        auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        if (user != null) {
            updateUserCredentials(user);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_container, InfosFragment.newInstance())
                    .commit();
            model.checkInfosUpdates();
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    private void updateUserCredentials(FirebaseUser user) {
        if (user.getPhotoUrl() != null) {
            Glide.with(this).load(user.getPhotoUrl()).into(((ImageView) navigationView.getHeaderView(0).findViewById(R.id.account_image)));
        }
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.account_name)).setText(user.getDisplayName());
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.account_email)).setText(user.getEmail());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_account) {

        } else if (id == R.id.nav_log_out) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.remove_account)
                    .setMessage(R.string.really_remove_account)
                    .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss()).setPositiveButton(R.string.yes, (dialog, which) -> {
                showEditDialog(getString(R.string.edit_password), getString(R.string.reauthenticate_user), (dialog1, text) -> {
                    if (text != null && text.length() > 7) {
                        dialog1.dismiss();
                        model.removeAccount(text).observe(this, statusResource -> {
                            if (statusResource != null) {
                                switch (statusResource.status) {
                                    case SUCCESS:
                                        dismissCurrentDialog();
                                        checkUserExist();
                                        break;
                                    case ERROR:
                                        showErrorDialog(statusResource.message);
                                        break;
                                    case LOADING:
                                        showLoadingDialog();
                                        break;
                                }
                            }
                        });
                    }
                });
            }).create().show();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Subscribe
    public void onEvent(Map<String, Object> event) {
        runOnUiThread(() -> {
            if (event.get(Constants.MESSAGE_TYPE).equals(Constants.STATUS_CHANGED)) {
//                updateInformationData();
            } else if (event.get(Constants.MESSAGE_TYPE).equals(Constants.OWNER_APPROVED)) {
                model.updateOtherInfo(event);
            }
        });
    }
}
