package com.infokeeper.infokeeper.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.fragments.LoginFragment;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.login_container, LoginFragment.newInstance())
                .commit();
    }
}
