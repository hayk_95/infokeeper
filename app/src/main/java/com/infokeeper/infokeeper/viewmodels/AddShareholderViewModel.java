package com.infokeeper.infokeeper.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.google.firebase.firestore.ListenerRegistration;
import com.infokeeper.infokeeper.data.DataRepository;
import com.infokeeper.infokeeper.data.Resource;
import com.infokeeper.infokeeper.data.StatusResource;
import com.infokeeper.infokeeper.data.models.Information;
import com.infokeeper.infokeeper.data.models.QRObject;
import com.infokeeper.infokeeper.data.models.Shareholder;
import com.infokeeper.infokeeper.data.models.XORSplitObject;

import java.security.SecureRandom;
import java.util.Map;

public class AddShareholderViewModel extends AndroidViewModel {

    private XORSplitObject xorSplitObject;
    private long createdTime;
    private Information information;
    private ListenerRegistration holdersRequestsListener;

    public AddShareholderViewModel(@NonNull Application application) {
        super(application);
        createdTime = System.currentTimeMillis();
    }

    public QRObject getInformationPart(String informationId) {
        information = DataRepository.getInstance(getApplication()).getInformationById(informationId);
        xorSplitObject = getXORSplit(information.getContent());
        return new QRObject(information.getId(), Base64.encodeToString(xorSplitObject.getHolderPart(), 0), information.getHoldersCount() + 1);
    }

    public LiveData<Resource<Shareholder>> checkShareholdersRequests() {
        Map<Integer, Object> list = DataRepository.getInstance(getApplication()).checkShareholdersRequests(information.getId(), information.getHoldersCount() + 1);
        holdersRequestsListener = ((ListenerRegistration) list.get(DataRepository.REGISTRATION_LISTENER_OBJECT));
        return ((LiveData<Resource<Shareholder>>) list.get(DataRepository.LIVE_DATA_OBJECT));
    }

    public LiveData<StatusResource> saveShareHolder(Shareholder shareholder) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        return Transformations.switchMap(DataRepository.getInstance(getApplication()).saveShareHolder(shareholder, information.getName()), input -> {
            if (input != null) {
                switch (input.status) {
                    case SUCCESS:
                        DataRepository.getInstance(getApplication()).updateOwnInfo(information.getId(),
                                Base64.encodeToString(xorSplitObject.getOwnPart(), 0),
                                information.getHoldersCount() + 1);
                        DataRepository.getInstance(getApplication()).removeShareholderRequests(information.getId());
                        liveData.postValue(StatusResource.success());
                        break;
                    case ERROR:
                        liveData.postValue(StatusResource.error(input.message));
                        break;
                    case LOADING:
                        liveData.postValue(StatusResource.loading());
                        break;
                }
            }
            return liveData;
        });
    }

    public XORSplitObject getXORSplit(String info) {

        byte[] infoBytes = Base64.decode(info, 0);

        byte[] ownPart = new byte[infoBytes.length];
        byte[] holderPart = new byte[infoBytes.length];

        new SecureRandom().nextBytes(ownPart);
        for (int i = 0; i < infoBytes.length; i++) {
            holderPart[i] = (byte) (infoBytes[i] ^ ownPart[i]);
        }

        return new XORSplitObject(ownPart, holderPart);
    }

    public long getCreatedTime() {
        return createdTime;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (holdersRequestsListener != null) {
            holdersRequestsListener.remove();
        }
    }
}
