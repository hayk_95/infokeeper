package com.infokeeper.infokeeper.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.google.firebase.firestore.ListenerRegistration;
import com.infokeeper.infokeeper.Constants;
import com.infokeeper.infokeeper.data.DataRepository;
import com.infokeeper.infokeeper.data.StatusResource;
import com.infokeeper.infokeeper.data.enums.InfoStatus;
import com.infokeeper.infokeeper.data.models.Information;

import java.util.List;
import java.util.Map;


public class InfosViewModel extends AndroidViewModel {

    private ListenerRegistration ownInfoListener;
    private ListenerRegistration otherInfoListener;

    public InfosViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<StatusResource> removeAccount(String password) {
        MutableLiveData<StatusResource> liveData = new MutableLiveData<>();
        return Transformations.switchMap(DataRepository.getInstance(getApplication()).removeAccount(password), input -> {
            if (input != null) {
                switch (input.status) {
                    case SUCCESS:
                        DataRepository.getInstance(getApplication()).removeAllData();
                        liveData.postValue(StatusResource.success());
                        break;
                    case ERROR:
                        liveData.postValue(StatusResource.error(input.message));
                        break;
                    case LOADING:
                        liveData.postValue(StatusResource.loading());
                        break;
                }
            }
            return liveData;
        });
    }

    public LiveData<List<Information>> getMyInformation() {
        return DataRepository.getInstance(getApplication()).getMyInformation();
    }

    public LiveData<List<Information>> getOtherInformation() {
        return DataRepository.getInstance(getApplication()).getOtherInformation();
    }

    public LiveData<StatusResource> requestInfosParts(Information information) {
        return DataRepository.getInstance(getApplication()).requestInfosParts(information);
    }

    public LiveData<StatusResource> sendInfoPart(Information information) {
        return DataRepository.getInstance(getApplication()).sendInfoPart(information);
    }

    public LiveData<StatusResource> denyRequest(Information information) {
        return DataRepository.getInstance(getApplication()).denyRequest(information);
    }

    public void checkInfosUpdates() {
        ownInfoListener = DataRepository.getInstance(getApplication()).checkOwnInfosUpdates();
        otherInfoListener = DataRepository.getInstance(getApplication()).checkOtherInfosUpdates();
    }

    public void updateOtherInfo(Map<String, Object> args) {
        Information information = new Information(((String) args.get(Constants.INFO_ID)),
                ((String) args.get(Constants.INFO_NAME)),
                ((String) args.get(Constants.OWNER_NAME)),
                ((InfoStatus) args.get(Constants.INFO_STATUS)));
        DataRepository.getInstance(getApplication()).updateOtherInfo(information);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (ownInfoListener != null) {
            ownInfoListener.remove();
            ownInfoListener = null;
        }
        if (otherInfoListener != null) {
            otherInfoListener.remove();
            otherInfoListener = null;
        }
    }
}
