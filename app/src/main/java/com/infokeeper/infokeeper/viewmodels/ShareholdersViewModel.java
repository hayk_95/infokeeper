package com.infokeeper.infokeeper.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.google.firebase.firestore.ListenerRegistration;
import com.infokeeper.infokeeper.data.DataRepository;
import com.infokeeper.infokeeper.data.Resource;
import com.infokeeper.infokeeper.data.StatusResource;
import com.infokeeper.infokeeper.data.enums.InfoStatus;
import com.infokeeper.infokeeper.data.models.Shareholder;

import java.util.ArrayList;
import java.util.Map;

public class ShareholdersViewModel extends AndroidViewModel {

    private ListenerRegistration shareholdersListener;

    public ShareholdersViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Resource<ArrayList<Shareholder>>> getShareholders(String infoId) {
        Map<Integer, Object> list = DataRepository.getInstance(getApplication()).getShareholders(infoId);
        shareholdersListener = ((ListenerRegistration) list.get(DataRepository.REGISTRATION_LISTENER_OBJECT));
        return ((LiveData<Resource<ArrayList<Shareholder>>>) list.get(DataRepository.LIVE_DATA_OBJECT));
    }

    public LiveData<StatusResource> sendRequest(Shareholder shareholder) {
        return DataRepository.getInstance(getApplication()).sendRequestToSingleHolder(shareholder);
    }

    public LiveData<StatusResource> loadInfoPart(Shareholder shareholder) {
        return DataRepository.getInstance(getApplication()).getInfoPart(shareholder);
    }

    public LiveData<StatusResource> updateInfoStatus(String infoId, InfoStatus status) {
        return DataRepository.getInstance(getApplication()).updateInfoStatus(infoId, status);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if(shareholdersListener != null){
            shareholdersListener.remove();
        }
    }
}
