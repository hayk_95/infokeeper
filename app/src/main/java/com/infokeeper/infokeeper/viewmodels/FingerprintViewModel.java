package com.infokeeper.infokeeper.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.infokeeper.infokeeper.data.DataRepository;
import com.infokeeper.infokeeper.data.StatusResource;

public class FingerprintViewModel extends AndroidViewModel {

    public FingerprintViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<StatusResource> checkUserPassword(String password) {
        return DataRepository.getInstance(getApplication()).reauthenticate(password);
    }
}
