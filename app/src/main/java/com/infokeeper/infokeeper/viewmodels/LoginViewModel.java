package com.infokeeper.infokeeper.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.utils.Utils;
import com.infokeeper.infokeeper.data.DataRepository;
import com.infokeeper.infokeeper.data.StatusResource;

public class LoginViewModel extends AndroidViewModel {

    private MutableLiveData<StatusResource> liveData;

    public LoginViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<StatusResource> createAccount(String name, String email, String password, String confirmPassword) {
        String message;
        if (name != null && !name.isEmpty() && email != null && password != null && !email.isEmpty() && !password.isEmpty() && confirmPassword != null && !confirmPassword.isEmpty()) {
            if (Utils.isValidEmail(email)) {
                if (password.length() > 7) {
                    if (password.equals(confirmPassword)) {
                        return DataRepository.getInstance(getApplication()).createAccount(name, email, password);
                    } else {
                        message = getApplication().getString(R.string.not_equal_passwords);
                    }
                } else {
                    message = getApplication().getString(R.string.enter_right_password);
                }
            } else {
                message = getApplication().getString(R.string.not_valid_email);
            }
        } else {
            message = getApplication().getString(R.string.fill_the_fields);
        }
        if (liveData == null) {
            liveData = new MutableLiveData<>();
        }
        liveData.postValue(StatusResource.error(message));
        return liveData;
    }
}
