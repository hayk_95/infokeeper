package com.infokeeper.infokeeper.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.infokeeper.infokeeper.Encryptor;
import com.infokeeper.infokeeper.data.DataRepository;
import com.infokeeper.infokeeper.data.StatusResource;

public class CreateInfoViewModel extends AndroidViewModel {

    private MutableLiveData<StatusResource> liveData;

    public CreateInfoViewModel(@NonNull Application application) {
        super(application);
        liveData = new MutableLiveData<>();
    }

    public LiveData<StatusResource> saveInfo(String name, String info) {
        String password = Encryptor.getInstance().randomPassword(8);
        return Transformations.switchMap(DataRepository.getInstance(getApplication()).addInfoExistingRepo(name, password), input -> {
            if (input != null) {
                switch (input.status) {
                    case SUCCESS:
                        Encryptor.getInstance().createNewKey(getApplication(), password);
                        String encrypteInfo = Encryptor.getInstance().encryptInfo(password, info);
                        if (encrypteInfo != null) {
                            DataRepository.getInstance(getApplication()).saveOwnInfo(((String) input.data), name, encrypteInfo);
                            liveData.postValue(StatusResource.success());
                        } else {
                            liveData.postValue(StatusResource.error(""));
                            DataRepository.getInstance(getApplication()).deleteInfo(((String) input.data));
                        }
                        break;
                    case ERROR:
                        liveData.postValue(StatusResource.error(input.message));
                        break;
                    case LOADING:
                        liveData.postValue(StatusResource.loading());
                        break;
                }
            }
            return liveData;
        });
    }
}
