package com.infokeeper.infokeeper.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.infokeeper.infokeeper.Constants;
import com.infokeeper.infokeeper.data.DataRepository;
import com.infokeeper.infokeeper.data.StatusResource;
import com.infokeeper.infokeeper.data.enums.InfoStatus;
import com.infokeeper.infokeeper.data.models.Information;
import com.infokeeper.infokeeper.data.models.QRObject;

import java.util.Map;

public class QRScannerViewModel extends AndroidViewModel {

    public QRScannerViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<StatusResource> addShareHolder(QRObject qrObject) {
        return DataRepository.getInstance(getApplication()).sendShareHolderRequest(qrObject);
    }

    public void updateOtherInfo(Map<String, Object> args) {
//        Information information = new Information(((String) args.get(Constants.INFO_ID)),
//                ((String) args.get(Constants.INFO_NAME)),
//                ((String) args.get(Constants.OWNER_NAME)),
//                ((InfoStatus) args.get(Constants.INFO_STATUS)));
//        DataRepository.getInstance(getApplication()).updateOtherInfo(information);
    }
}
