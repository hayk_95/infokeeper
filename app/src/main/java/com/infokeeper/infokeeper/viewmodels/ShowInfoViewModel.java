package com.infokeeper.infokeeper.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.infokeeper.infokeeper.Encryptor;
import com.infokeeper.infokeeper.data.DataRepository;
import com.infokeeper.infokeeper.data.Resource;
import com.infokeeper.infokeeper.data.models.Information;
import com.infokeeper.infokeeper.data.models.SeparateInfo;

import java.util.Collections;
import java.util.List;

public class ShowInfoViewModel extends AndroidViewModel {

    public ShowInfoViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Resource<String>> getDecrypteInfo(String infoId) {
        MutableLiveData<Resource<String>> liveData = new MutableLiveData<>();
        return Transformations.switchMap(DataRepository.getInstance(getApplication()).getInfoKeyPassword(infoId), input -> {
            if (input != null) {
                switch (input.status) {
                    case SUCCESS:
                        List<SeparateInfo> infoParts = DataRepository.getInstance(getApplication()).getSeparateInfoParts(infoId);
                        Information information = DataRepository.getInstance(getApplication()).getInformationById(infoId);
                        Collections.sort(infoParts, (o1, o2) -> o1.getPart() - o2.getPart());
                        byte[][] parts = new byte[infoParts.size()][];
                        for (int i = 0; i < parts.length; i++) {
                            parts[i] = Base64.decode(infoParts.get(i).getContent(), 0);
                        }
                        String initialEncrypteInfo = Base64.encodeToString(XORPartsMerge(Base64.decode(information.getContent(), 0), parts), 0);
                        String decrypteInfo = Encryptor.getInstance().decryptInfo(input.data, initialEncrypteInfo);
                        liveData.postValue(Resource.success(decrypteInfo));
                        break;
                    case ERROR:
                        liveData.postValue(Resource.error(input.message, null));
                        break;
                    case LOADING:
                        liveData.postValue(Resource.loading(null));
                        break;
                }
            }
            return liveData;
        });
    }

    private byte[] XORPartsMerge(byte[] parent, byte[][] parts) {
        byte[] out = new byte[parent.length];
        System.arraycopy(parent, 0, out, 0, parent.length);
        for (int i = parts.length - 1; i >= 0; i--) {
            for (int j = 0; j < out.length; j++) {
                out[j] ^= parts[i][j];
            }
        }
        return out;
    }
}
