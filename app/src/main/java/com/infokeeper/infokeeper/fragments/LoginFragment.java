package com.infokeeper.infokeeper.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.activities.MainActivity;
import com.infokeeper.infokeeper.viewmodels.LoginViewModel;


public class LoginFragment extends BaseFragment {

    private EditText editName;
    private EditText editEmail;
    private EditText editPassword;
    private EditText editConfirmPassword;
    private TextView login;

    private LoginViewModel model;

    public static LoginFragment newInstance() {

        Bundle args = new Bundle();
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setListeners();
        bindModelData();
    }

    private void initViews(View view) {
        editName = view.findViewById(R.id.edit_name);
        editEmail = view.findViewById(R.id.edit_email);
        editPassword = view.findViewById(R.id.edit_password);
        editConfirmPassword = view.findViewById(R.id.edit_confirm_password);
        login = view.findViewById(R.id.login);
    }

    private void setListeners() {
        login.setOnClickListener(v ->
                model.createAccount(editName.getText().toString(), editEmail.getText().toString(), editPassword.getText().toString(), editConfirmPassword.getText().toString()).observe(this, statusResource -> {
                    if (statusResource != null) {
                        switch (statusResource.status) {
                            case SUCCESS:
                                dismissCurrentDialog();
                                startActivity(new Intent(getContext(), MainActivity.class));
                                getActivity().finish();
                                break;
                            case ERROR:
                                dismissCurrentDialog();
                                Toast.makeText(getContext(), statusResource.message != null ? statusResource.message : getString(R.string.error), Toast.LENGTH_SHORT).show();
                                break;
                            case LOADING:
                                showLoadingDialog();
                                break;
                        }
                    }
                }));
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(LoginViewModel.class);
    }
}
