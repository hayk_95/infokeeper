package com.infokeeper.infokeeper.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;

import com.infokeeper.infokeeper.utils.Utils;
import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.fragments.dialogs.FingerprintDialogFragment;
import com.infokeeper.infokeeper.fragments.dialogs.LoadingDialogFragment;

import java.util.Objects;

public class BaseFragment extends Fragment {
    public static final String LOADING_TAG = "loading";

    private LoadingDialogFragment loadingDialog;

    private int pendingLoadingCount = 0;


    public void showLoadingDialog() {
        if (pendingLoadingCount == 0) {
            dismissCurrentDialog();
            loadingDialog = LoadingDialogFragment.newInstance();
            FragmentManager manager = Objects.requireNonNull(getFragmentManager());
            loadingDialog.show(manager, LOADING_TAG);
        }
        pendingLoadingCount++;
    }

    public void dismissCurrentDialog() {
        if (Objects.requireNonNull(getFragmentManager()).findFragmentByTag(LOADING_TAG) != null) {
            if (pendingLoadingCount > 0) {
                pendingLoadingCount--;
            }
            if (pendingLoadingCount == 0) {
                loadingDialog.dismiss();
            }
        }
    }

    public void showErrorDialog(String errorMsg) {
        dismissCurrentDialog();
        String errorMessage = Utils.isNetworkConnected(getActivity())
                ? (errorMsg != null && !errorMsg.isEmpty()) ? errorMsg : getString(R.string.default_error_message)
                : getString(R.string.no_connection_error_message);
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(errorMessage)
                .setPositiveButton(getString(R.string.ok), (dialog, which) -> dialog.dismiss())
                .create().show();
    }

    public void openFingerprintDialog(FingerprintDialogFragment.OnFingerprintEventListener eventListener) {
        FingerprintDialogFragment.newInstance(eventListener).show(getChildFragmentManager(), null);
    }
}
