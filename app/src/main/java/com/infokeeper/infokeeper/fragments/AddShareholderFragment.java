package com.infokeeper.infokeeper.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.infokeeper.infokeeper.utils.QRUtils;
import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.data.models.QRObject;
import com.infokeeper.infokeeper.data.models.Shareholder;
import com.infokeeper.infokeeper.viewmodels.AddShareholderViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Objects;

public class AddShareholderFragment extends BaseFragment {

    private static final String INFORMATION_ID_ARGUMENT = "informationId";

    private ImageView qrImage;
    private Button cancel;

    private AddShareholderViewModel model;

    public static AddShareholderFragment newInstance(String informationId) {

        Bundle args = new Bundle();
        args.putString(INFORMATION_ID_ARGUMENT, informationId);
        AddShareholderFragment fragment = new AddShareholderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_share_holder, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model = ViewModelProviders.of(this).get(AddShareholderViewModel.class);
        initUI(view);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void initUI(View view) {
        qrImage = view.findViewById(R.id.qr_image);
        cancel = view.findViewById(R.id.cancel);
        QRObject qrObject = model.getInformationPart(Objects.requireNonNull(getArguments()).getString(INFORMATION_ID_ARGUMENT));
        model.checkShareholdersRequests().observe(this, shareholderResource -> {
            if (shareholderResource != null) {
                switch (shareholderResource.status) {
                    case SUCCESS:
                        new AlertDialog.Builder(getContext())
                                .setMessage(shareholderResource.data.getName() + " " + getString(R.string.can_approve))
                                .setNegativeButton(R.string.no, (dialog, which) -> dialog.dismiss())
                                .setPositiveButton(R.string.yes, (dialog, which) -> {
                                    model.saveShareHolder(shareholderResource.data).observe(AddShareholderFragment.this, statusResource -> {
                                        if (statusResource != null) {
                                            switch (statusResource.status) {
                                                case SUCCESS:
                                                    dismissCurrentDialog();
                                                    Toast.makeText(getContext(), R.string.shareholder_saved, Toast.LENGTH_LONG).show();
//                                                    getFragmentManager().popBackStack();
                                                    break;
                                                case ERROR:
                                                    showErrorDialog(statusResource.message);
                                                    break;
                                                case LOADING:
                                                    showLoadingDialog();
                                                    break;
                                            }
                                        }
                                    });
                                }).create().show();
                        break;
                    case ERROR:
                        break;
                    case LOADING:
                        break;
                }
            }
        });
        QRUtils.generate(qrImage, qrObject);
        cancel.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager().popBackStack();
        });
    }

    @Subscribe
    public void onEvent(Shareholder shareholder) {
        Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
            if (shareholder.getInfoId().equals(Objects.requireNonNull(getArguments()).getString(INFORMATION_ID_ARGUMENT)) && model.getCreatedTime() < shareholder.getCreatedTime()) {
//                new AlertDialog.Builder(getContext())
//                        .setMessage(shareholder.getName() + " " + getString(R.string.can_approve))
//                        .setNegativeButton(R.string.no,(dialog, which) -> dialog.dismiss())
//                        .setPositiveButton(R.string.yes,(dialog, which) -> {
//                            model.saveShareHolder(shareholder).observe(AddShareholderFragment.this,statusResource -> {
//                                if(statusResource != null){
//                                    switch (statusResource.status){
//                                        case SUCCESS:
//                                            dismissCurrentDialog();
//                                            Toast.makeText(getContext(), "Shareholder saved success", Toast.LENGTH_LONG).show();
//                                            getFragmentManager().popBackStack();
//                                            break;
//                                        case ERROR:
//                                            showErrorDialog(statusResource.message);
//                                            break;
//                                        case LOADING:
//                                            showLoadingDialog();
//                                            break;
//                                    }
//                                }
//                            });
//                        }).create().show();
            }
        });
    }
}
