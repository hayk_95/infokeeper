package com.infokeeper.infokeeper.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.viewmodels.ShowInfoViewModel;

public class ShowInfoFragment extends BaseFragment {

    private static final String INFO_ID_ARGUMENT = "infoIdArgument";
    private static final String INFO_NAME_ARGUMENT = "infoNameArgument";

    private TextView infoName;
    private TextView infoText;

    private ShowInfoViewModel model;

    public static ShowInfoFragment newInstance(String infoId, String infoName) {

        Bundle args = new Bundle();
        args.putString(INFO_ID_ARGUMENT, infoId);
        args.putString(INFO_NAME_ARGUMENT, infoName);
        ShowInfoFragment fragment = new ShowInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_show_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        infoName = view.findViewById(R.id.info_name);
        infoText = view.findViewById(R.id.secret_info);
        infoName.setText(getArguments().getString(INFO_NAME_ARGUMENT));
        bindModelData();
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(ShowInfoViewModel.class);
        model.getDecrypteInfo(getArguments().getString(INFO_ID_ARGUMENT)).observe(this, infoResource -> {
            if (infoResource != null) {
                switch (infoResource.status) {
                    case SUCCESS:
                        dismissCurrentDialog();
                        infoText.setText(infoResource.data);
                        Toast.makeText(getContext(), infoResource.data, Toast.LENGTH_SHORT).show();
                        break;
                    case ERROR:
                        showErrorDialog(infoResource.message);
                        break;
                    case LOADING:
                        showLoadingDialog();
                        break;
                }
            }
        });
    }
}
