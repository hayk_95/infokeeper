package com.infokeeper.infokeeper.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.adapters.InfosAdapter;
import com.infokeeper.infokeeper.data.models.Information;
import com.infokeeper.infokeeper.viewmodels.InfosViewModel;

public class MyInfosFragment extends BaseFragment {

    private RecyclerView infosList;
    private TextView popupText;

    private InfosAdapter adapter;
    private InfosViewModel model;

    public static MyInfosFragment newInstance() {

        Bundle args = new Bundle();
        MyInfosFragment fragment = new MyInfosFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_infos, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
        bindModelData();
    }

    private void initUI(View view) {
        infosList = view.findViewById(R.id.infos_list);
        popupText = view.findViewById(R.id.popup_text);
        adapter = new InfosAdapter(true);
        infosList.setLayoutManager(new LinearLayoutManager(getContext()));
        infosList.setAdapter(adapter);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(InfosViewModel.class);
        model.getMyInformation().observe(this, list -> {
            adapter.updateInformation(list);
            popupText.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        });
        adapter.setOnInformationItemEventListener(new InfosAdapter.OnInformationItemEventListener() {
            @Override
            public void onInformationClicked(Information information) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.main_container, ShareholdersFragment.newInstance(information.getId(), information.getName(), information.getStatus(), information.isRequested()))
                        .commit();
            }

            @Override
            public void onInformationMoreClicked(Information information) {
                String[] options = new String[information.getHoldersCount() != 0 ? 2 : 1];
                options[0] = getString(R.string.add_shareholder);
                if (information.getHoldersCount() != 0) {
                    options[1] = getString(R.string.request_information);
                }
                new AlertDialog.Builder(getContext())
                        .setTitle(getString(R.string.what_to_do))
                        .setSingleChoiceItems(options, -1, null)
                        .setNegativeButton(R.string.cancel, null)
                        .setPositiveButton(R.string.ok, (dialog, which) -> {
                            dialog.dismiss();
                            openFingerprintDialog(() -> {
                                switch (((AlertDialog) dialog).getListView().getCheckedItemPosition()) {
                                    case 0:
                                        getActivity().getSupportFragmentManager().beginTransaction()
                                                .addToBackStack(null)
                                                .replace(R.id.main_container, AddShareholderFragment.newInstance(information.getId()))
                                                .commit();
                                        break;
                                    case 1:
                                        model.requestInfosParts(information).observe(MyInfosFragment.this, statusResource -> {
                                            if (statusResource != null) {
                                                switch (statusResource.status) {
                                                    case SUCCESS:
                                                        dismissCurrentDialog();
                                                        Toast.makeText(getContext(), R.string.request_sent, Toast.LENGTH_SHORT).show();
                                                        break;
                                                    case ERROR:
                                                        showErrorDialog(statusResource.message);
                                                        break;
                                                    case LOADING:
                                                        showLoadingDialog();
                                                        break;
                                                }
                                            }
                                        });
                                        break;
                                }
                            });
                        }).create().show();
            }
        });
    }
}
