package com.infokeeper.infokeeper.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.infokeeper.infokeeper.Constants;
import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.adapters.ShareholdersAdapter;
import com.infokeeper.infokeeper.data.enums.InfoStatus;
import com.infokeeper.infokeeper.data.models.Shareholder;
import com.infokeeper.infokeeper.viewmodels.ShareholdersViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Map;

public class ShareholdersFragment extends BaseFragment {
    private static final String INFO_ID_ARGUMENT = "infoIdArgument";
    private static final String INFO_NAME_ARGUMENT = "infoNameArgument";
    private static final String INFO_STATUS_ARGUMENT = "infoStatusArgument";
    private static final String INFO_IS_REQUESTED_ARGUMENT = "infoIsRequestedArgument";

    private RecyclerView holdersList;
    private TextView infoName;
    private LinearLayout popupLayout;
    private TextView popupText;
    private TextView showInfo;

    private ShareholdersViewModel model;
    private ShareholdersAdapter adapter;

    public static ShareholdersFragment newInstance(String infoId, String infoName, InfoStatus infoStatus, boolean isRequested) {

        Bundle args = new Bundle();
        args.putString(INFO_ID_ARGUMENT, infoId);
        args.putString(INFO_NAME_ARGUMENT, infoName);
        args.putSerializable(INFO_STATUS_ARGUMENT, infoStatus);
        args.putBoolean(INFO_IS_REQUESTED_ARGUMENT, isRequested);
        ShareholdersFragment fragment = new ShareholdersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shareholders, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
        setListeners();
        bindModelData();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void initUI(View view) {
        holdersList = view.findViewById(R.id.holders_list);
        infoName = view.findViewById(R.id.info_name);
        popupLayout = view.findViewById(R.id.popup_layout);
        popupText = view.findViewById(R.id.popup_text);
        showInfo = view.findViewById(R.id.show_info);
        infoName.setText(getArguments().getString(INFO_NAME_ARGUMENT));
        adapter = new ShareholdersAdapter(getArguments().getBoolean(INFO_IS_REQUESTED_ARGUMENT));
        holdersList.setLayoutManager(new LinearLayoutManager(getContext()));
        holdersList.setAdapter(adapter);
    }

    private void setListeners() {
        adapter.setOnShareholderItemEventListener(shareholder -> {
            openFingerprintDialog(() -> {
                switch (shareholder.getStatus()) {
                    case HOLD:
                        sendRequest(shareholder);
                        break;
                    case DELETED:
                        new AlertDialog.Builder(getContext())
                                .setMessage(R.string.load_deleted_info_part)
                                .setNegativeButton(R.string.cancel, null)
                                .setPositiveButton(R.string.yes, (dialog, which) -> {
                                    dialog.dismiss();
                                    loadInfoPart(shareholder);
                                }).create().show();
                        break;
                    case DENY:
                        sendRequest(shareholder);
                        break;
                    case APPROVED:
                        new AlertDialog.Builder(getContext())
                                .setMessage(R.string.load_info_part)
                                .setNegativeButton(R.string.cancel, null)
                                .setPositiveButton(R.string.yes, (dialog, which) -> {
                                    dialog.dismiss();
                                    loadInfoPart(shareholder);
                                }).create().show();
                        break;
                }
            });
        });

        showInfo.setOnClickListener(v -> {
            openFingerprintDialog(() ->
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .addToBackStack(null)
                            .replace(R.id.main_container, ShowInfoFragment.newInstance(getArguments().getString(INFO_ID_ARGUMENT), getArguments().getString(INFO_NAME_ARGUMENT)))
                            .commit());
        });
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(ShareholdersViewModel.class);
        model.getShareholders(getArguments().getString(INFO_ID_ARGUMENT)).observe(this, shareholdersResource -> {
            if (shareholdersResource != null) {
                switch (shareholdersResource.status) {
                    case SUCCESS:
                        dismissCurrentDialog();
                        adapter.updateShareholders(shareholdersResource.data);
                        if (adapter.getItemCount() == 0) {
                            showPopup(true, (getArguments().getSerializable(INFO_STATUS_ARGUMENT)) == InfoStatus.NOT_SHARED);
                        }
                        updateInfoStatus();
                        break;
                    case ERROR:
                        dismissCurrentDialog();
                        if(shareholdersResource.data != null){
                            adapter.deleteShareHolder(shareholdersResource.data.get(0));
                        }else {
                            showErrorDialog(shareholdersResource.message);
                        }
                        break;
                    case LOADING:
                        showLoadingDialog();
                        break;
                }
            }
        });
    }

    private void updateInfoStatus() {
        InfoStatus status = adapter.getCurrentInfoStatus();
        if (status != InfoStatus.NOT_SHARED && status != getArguments().getSerializable(INFO_STATUS_ARGUMENT)) {
            model.updateInfoStatus(getArguments().getString(INFO_ID_ARGUMENT), status);
        }
    }

    private void sendRequest(Shareholder shareholder) {
        new AlertDialog.Builder(getContext())
                .setMessage(getString(R.string.send_request_to_holder) + " " + shareholder.getName())
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    model.sendRequest(shareholder).observe(this, statusResource -> {
                        if (statusResource != null) {
                            switch (statusResource.status) {
                                case SUCCESS:
                                    dismissCurrentDialog();
                                    shareholder.setStatus(InfoStatus.REQUESTED);
                                    adapter.notifyDataSetChanged();
                                    break;
                                case ERROR:
                                    showErrorDialog(statusResource.message);
                                    break;
                                case LOADING:
                                    showLoadingDialog();
                                    break;
                            }
                        }
                    });
                }).create().show();
    }

    private void loadInfoPart(Shareholder shareholder) {
        model.loadInfoPart(shareholder).observe(this, statusResource -> {
            if (statusResource != null) {
                switch (statusResource.status) {
                    case SUCCESS:
                        dismissCurrentDialog();
                        adapter.removeShareholder(shareholder);
                        if (adapter.getItemCount() == 0) {
                            showPopup(true, false);
                        }
                        updateInfoStatus();
                        break;
                    case ERROR:
                        showErrorDialog(statusResource.message);
                        break;
                    case LOADING:
                        showLoadingDialog();
                        break;
                }
            }
        });
    }

    private void showPopup(boolean show, boolean isWarning) {
        if (show) {
            popupText.setText(isWarning ? R.string.not_share_info : R.string.info_come_together);
            popupText.setTextColor(getResources().getColor(isWarning ? R.color.red : R.color.green));
            popupLayout.setVisibility(View.VISIBLE);
        } else {
            popupLayout.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onEvent(Map<String, Object> event) {
        getActivity().runOnUiThread(() -> {
            if (event.get(Constants.MESSAGE_TYPE).equals(Constants.STATUS_CHANGED)) {
//                adapter.updateShareHolderStatus(((Integer) event.get(Constants.INFO_PART)), ((String) event.get(Constants.INFO_STATUS)));
            }
        });
    }
}
