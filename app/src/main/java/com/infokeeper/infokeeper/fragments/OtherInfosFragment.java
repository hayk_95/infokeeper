package com.infokeeper.infokeeper.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.adapters.InfosAdapter;
import com.infokeeper.infokeeper.data.enums.InfoStatus;
import com.infokeeper.infokeeper.data.models.Information;
import com.infokeeper.infokeeper.viewmodels.InfosViewModel;

public class OtherInfosFragment extends BaseFragment {

    private RecyclerView infosList;
    private TextView popupText;

    private InfosAdapter adapter;
    private InfosViewModel model;

    public static OtherInfosFragment newInstance() {

        Bundle args = new Bundle();

        OtherInfosFragment fragment = new OtherInfosFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_other_infos, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
        bindModelData();
    }

    private void initUI(View view) {
        infosList = view.findViewById(R.id.infos_list);
        popupText = view.findViewById(R.id.popup_text);
        adapter = new InfosAdapter(false);
        infosList.setLayoutManager(new LinearLayoutManager(getContext()));
        infosList.setAdapter(adapter);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(InfosViewModel.class);
        model.getOtherInformation().observe(this, list -> {
            adapter.updateInformation(list);
            popupText.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        });
        adapter.setOnInformationItemEventListener(new InfosAdapter.OnInformationItemEventListener() {
            @Override
            public void onInformationClicked(Information information) {
                String[] options = new String[information.getStatus() == InfoStatus.REQUESTED ? 2 : 1];
                options[0] = getString(information.getStatus() == InfoStatus.REQUESTED ? R.string.send_info : R.string.delete_information);
                if (information.getStatus() == InfoStatus.REQUESTED) {
                    options[1] = getString(R.string.deny_request);
                }
                new AlertDialog.Builder(getContext())
                        .setTitle(getString(R.string.what_to_do))
                        .setSingleChoiceItems(options, -1, null)
                        .setNegativeButton(R.string.cancel, null)
                        .setPositiveButton(R.string.ok, (dialog, which) -> {
                            dialog.dismiss();
                            openFingerprintDialog(()->{
                                switch (((AlertDialog) dialog).getListView().getCheckedItemPosition()) {
                                    case 0:
                                        if (information.getStatus() == InfoStatus.REQUESTED) {
                                            information.setStatus(InfoStatus.APPROVED);
                                            sendInfoPart(information);
                                        } else {
                                            new AlertDialog.Builder(getContext())
                                                    .setMessage(R.string.really_delete_info_part)
                                                    .setNegativeButton(R.string.cancel, null)
                                                    .setPositiveButton(R.string.yes, (dialog1, which1) -> {
                                                        dialog1.dismiss();
                                                        information.setStatus(InfoStatus.DELETED);
                                                        sendInfoPart(information);
                                                    }).create().show();
                                        }
                                        break;
                                    case 1:
                                        model.denyRequest(information).observe(OtherInfosFragment.this, statusResource -> {
                                            if (statusResource != null) {
                                                switch (statusResource.status) {
                                                    case SUCCESS:
                                                        dismissCurrentDialog();
                                                        break;
                                                    case ERROR:
                                                        showErrorDialog(statusResource.message);
                                                        break;
                                                    case LOADING:
                                                        showLoadingDialog();
                                                        break;
                                                }
                                            }
                                        });
                                        break;
                                }
                            });

                        }).create().show();
            }

            @Override
            public void onInformationMoreClicked(Information information) {
            }
        });
    }

    private void sendInfoPart(Information information) {
        model.sendInfoPart(information).observe(this, statusResource -> {
            if (statusResource != null) {
                switch (statusResource.status) {
                    case SUCCESS:
                        dismissCurrentDialog();
                        Toast.makeText(getContext(), information.getStatus() == InfoStatus.REQUESTED ?
                                R.string.info_part_sent :
                                R.string.info_part_deleted, Toast.LENGTH_SHORT).show();
                        break;
                    case ERROR:
                        showErrorDialog(statusResource.message);
                        break;
                    case LOADING:
                        showLoadingDialog();
                        break;
                }
            }
        });
    }
}
