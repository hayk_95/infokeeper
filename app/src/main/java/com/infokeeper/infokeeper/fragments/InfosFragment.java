package com.infokeeper.infokeeper.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.activities.QRScannerActivity;

public class InfosFragment extends Fragment {
    private FloatingActionButton addButton;
    private ViewPager infosPager;
    private TabLayout infosTabs;

    public static InfosFragment newInstance() {

        Bundle args = new Bundle();
        InfosFragment fragment = new InfosFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_infos, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setListeners();
    }

    private void initViews(View view) {
        addButton = view.findViewById(R.id.add_button);
        infosPager = view.findViewById(R.id.view_pager);
        infosTabs = view.findViewById(R.id.tabs_layout);
        infosPager.setAdapter(new InfosPagerAdapter(getChildFragmentManager()));
        infosTabs.setupWithViewPager(infosPager);
    }

    private void setListeners() {
        addButton.setOnClickListener(view -> {
            new AlertDialog.Builder(getContext())
                    .setTitle(getString(R.string.what_to_do))
                    .setSingleChoiceItems(new String[]{getString(R.string.add_my_info), getString(R.string.add_other_info)}, -1, null)
                    .setNegativeButton(R.string.cancel, null)
                    .setPositiveButton(R.string.ok, (dialog, which) -> {
                        dialog.dismiss();
                        switch (((AlertDialog) dialog).getListView().getCheckedItemPosition()) {
                            case 0:
                                getFragmentManager().beginTransaction()
                                        .addToBackStack(null)
                                        .replace(R.id.main_container, CreateInfoFragment.newInstance())
                                        .commit();
                                break;
                            case 1:
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                                dialogBuilder.setMessage(R.string.join_secret_title);
                                dialogBuilder.setPositiveButton(getString(R.string.ok), (dialogInterface, i) -> {
                                    dialogInterface.dismiss();
                                    startActivity(new Intent(getActivity(), QRScannerActivity.class));
                                }).create().show();
                                break;
                        }
                    }).create().show();
        });
    }

    private class InfosPagerAdapter extends FragmentStatePagerAdapter {


        public InfosPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return MyInfosFragment.newInstance();
                case 1:
                    return OtherInfosFragment.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.my_infos);
                case 1:
                    return getString(R.string.other_infos);
            }
            return "";
        }
    }
}
