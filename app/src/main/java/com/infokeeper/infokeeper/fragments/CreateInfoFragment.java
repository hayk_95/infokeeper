package com.infokeeper.infokeeper.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.viewmodels.CreateInfoViewModel;

public class CreateInfoFragment extends BaseFragment {

    private EditText nameEdit;
    private EditText informationEdit;
    private TextView save;

    private CreateInfoViewModel model;

    public static CreateInfoFragment newInstance() {

        Bundle args = new Bundle();

        CreateInfoFragment fragment = new CreateInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        model = ViewModelProviders.of(this).get(CreateInfoViewModel.class);
        nameEdit = view.findViewById(R.id.name_edit);
        informationEdit = view.findViewById(R.id.info_edit);
        save = view.findViewById(R.id.save_info);
        save.setOnClickListener(v -> {
            if (!nameEdit.getText().toString().isEmpty() && !informationEdit.getText().toString().isEmpty()) {
                model.saveInfo(nameEdit.getText().toString(), informationEdit.getText().toString()).observe(this, statusResource -> {
                    if (statusResource != null) {
                        switch (statusResource.status) {
                            case SUCCESS:
                                dismissCurrentDialog();
                                new AlertDialog.Builder(getActivity())
                                        .setTitle(R.string.save_success)
                                        .setMessage(R.string.add_shareholders)
                                        .setPositiveButton(R.string.ok, (dialog, which) -> {
                                            dialog.dismiss();
                                            getFragmentManager().popBackStack();
                                        })
                                        .create().show();
                                break;
                            case ERROR:
                                showErrorDialog(statusResource.message);
                                break;
                            case LOADING:
                                showLoadingDialog();
                                break;
                        }
                    }
                });
            } else {
                Toast.makeText(getContext(), R.string.fill_info, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
