package com.infokeeper.infokeeper.fragments.dialogs;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.infokeeper.infokeeper.utils.FingerprintUiHelper;
import com.infokeeper.infokeeper.R;
import com.infokeeper.infokeeper.viewmodels.FingerprintViewModel;

import javax.annotation.Nullable;

public class FingerprintDialogFragment extends DialogFragment
        implements TextView.OnEditorActionListener, FingerprintUiHelper.Callback {

    private Button cancelButton;
    private Button secondDialogButton;
    private View fingerprintContent;
    private View backupContent;
    private EditText password;
    private TextView addFingerprint;
    private ProgressBar progressBar;

    private Stage stage = Stage.FINGERPRINT;

    private FingerprintUiHelper fingerprintUiHelper;

    private InputMethodManager inputMethodManager;
    private OnFingerprintEventListener eventListener;
    private FingerprintViewModel model;

    public static FingerprintDialogFragment newInstance(OnFingerprintEventListener eventListener) {

        Bundle args = new Bundle();
        FingerprintDialogFragment fragment = new FingerprintDialogFragment();
        fragment.setArguments(args);
        fragment.setOnFingerprintEventListener(eventListener);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        inputMethodManager = context.getSystemService(InputMethodManager.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.authenticate));
        setCancelable(false);
        return inflater.inflate(R.layout.fragment_fingerprint_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initUI(view);
        setListeners();
    }

    private void initViews(View view) {
        cancelButton = view.findViewById(R.id.cancel_button);
        secondDialogButton = view.findViewById(R.id.second_dialog_button);
        fingerprintContent = view.findViewById(R.id.fingerprint_container);
        backupContent = view.findViewById(R.id.backup_container);
        password = view.findViewById(R.id.password);
        addFingerprint = view.findViewById(R.id.add_fingerprint);
        progressBar = view.findViewById(R.id.progress_bar);
    }

    private void initUI(View view) {
        model = ViewModelProviders.of(this).get(FingerprintViewModel.class);
        password.setOnEditorActionListener(this);
        fingerprintUiHelper = new FingerprintUiHelper(
                getActivity().getSystemService(FingerprintManager.class),
                view.findViewById(R.id.fingerprint_icon),
                view.findViewById(R.id.fingerprint_status), this);
        updateStage();

        if (!(fingerprintUiHelper.isFingerprintAuthAvailable() &&
                fingerprintUiHelper.isExistFingerprint())) {
            goToBackup();
        }
    }

    private void setListeners() {
        cancelButton.setOnClickListener(view -> dismiss());

        secondDialogButton.setOnClickListener(view -> {
            if (stage == Stage.FINGERPRINT) {
                goToBackup();
            } else {
                verifyPassword();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (stage == Stage.FINGERPRINT) {
            fingerprintUiHelper.startListening();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        fingerprintUiHelper.stopListening();
    }

    private void goToBackup() {
        stage = Stage.PASSWORD;
        updateStage();
        password.requestFocus();

        password.postDelayed(showKeyboardRunnable, 500);

        fingerprintUiHelper.stopListening();
        addFingerprint.setVisibility(fingerprintUiHelper.isFingerprintAuthAvailable() && !fingerprintUiHelper.isExistFingerprint() ? View.VISIBLE : View.GONE);
    }

    private void verifyPassword() {
        inputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        if (password.getText().toString().isEmpty() || password.getText().toString().length() < 8) {
            Toast.makeText(getContext(), "Not valid password", Toast.LENGTH_SHORT).show();
            return;
        }
        model.checkUserPassword(password.getText().toString()).observe(this, statusResource -> {
            if (statusResource != null) {
                switch (statusResource.status) {
                    case SUCCESS:
                        progressBar.setVisibility(View.GONE);
                        if (eventListener != null) {
                            eventListener.onAuthenticated();
                        }
                        dismiss();
                        break;
                    case ERROR:
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "Not valid password", Toast.LENGTH_SHORT).show();
                        break;
                    case LOADING:
                        progressBar.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });
    }

    private final Runnable showKeyboardRunnable = new Runnable() {
        @Override
        public void run() {
            inputMethodManager.showSoftInput(password, 0);
        }
    };

    private void updateStage() {
        switch (stage) {
            case FINGERPRINT:
                cancelButton.setText(R.string.cancel);
                secondDialogButton.setText(R.string.use_password);
                fingerprintContent.setVisibility(View.VISIBLE);
                backupContent.setVisibility(View.GONE);
                break;
            case PASSWORD:
                cancelButton.setText(R.string.cancel);
                secondDialogButton.setText(R.string.ok);
                fingerprintContent.setVisibility(View.GONE);
                backupContent.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_GO) {
            verifyPassword();
            return true;
        }
        return false;
    }

    @Override
    public void onAuthenticated() {
        if (eventListener != null) {
            eventListener.onAuthenticated();
        }
        dismiss();
    }

    @Override
    public void onError() {
        goToBackup();
    }

    public interface OnFingerprintEventListener {
        void onAuthenticated();
    }

    public void setOnFingerprintEventListener(OnFingerprintEventListener eventListener) {
        this.eventListener = eventListener;
    }

    public enum Stage {
        FINGERPRINT,
        PASSWORD
    }
}
