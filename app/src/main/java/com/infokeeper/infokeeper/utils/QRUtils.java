package com.infokeeper.infokeeper.utils;

import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.infokeeper.infokeeper.data.models.QRObject;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONException;
import org.json.JSONObject;

public class QRUtils {


    public static void generate(ImageView imageView, QRObject qrObject) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(qrObject.toString(), BarcodeFormat.QR_CODE, 200, 200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            imageView.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public static void scan(int requestCode, int resultCode, Intent data, QRScanListener qrScanListener) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                qrScanListener.onScanFail();
            } else {
                try {
                    QRObject qrObject = QRObject.parse(new JSONObject(result.getContents()));
                    qrScanListener.onScanSuccess(qrObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                    qrScanListener.onScanFail();
                }
            }
        } else {
            qrScanListener.onScanFail();
        }
    }


    public interface QRScanListener {
        void onScanSuccess(QRObject qrObject);

        void onScanFail();
    }
}
