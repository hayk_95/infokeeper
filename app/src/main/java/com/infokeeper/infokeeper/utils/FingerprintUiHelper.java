package com.infokeeper.infokeeper.utils;

import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.widget.ImageView;
import android.widget.TextView;

import com.infokeeper.infokeeper.R;

public class FingerprintUiHelper extends FingerprintManager.AuthenticationCallback {

    private static final long ERROR_TIMEOUT_MILLIS = 1600;
    private static final long SUCCESS_DELAY_MILLIS = 1300;

    private final FingerprintManager fingerprintManager;
    private final ImageView icon;
    private final TextView errorTextView;
    private final Callback callback;
    private CancellationSignal cancellationSignal;

    private boolean selfCancelled;

    public FingerprintUiHelper(FingerprintManager fingerprintManager,
                               ImageView icon, TextView errorTextView, Callback callback) {
        this.fingerprintManager = fingerprintManager;
        this.icon = icon;
        this.errorTextView = errorTextView;
        this.callback = callback;
    }

    public boolean isFingerprintAuthAvailable() {
        return fingerprintManager.isHardwareDetected();
    }

    public boolean isExistFingerprint() {
        return fingerprintManager.hasEnrolledFingerprints();
    }

    public void startListening() {
        if (!(isFingerprintAuthAvailable() && isExistFingerprint())) {
            return;
        }
        cancellationSignal = new CancellationSignal();
        selfCancelled = false;
        fingerprintManager.authenticate(null, cancellationSignal, 0 /* flags */, this, null);
        icon.setImageResource(R.drawable.ic_fp_40px);
    }

    public void stopListening() {
        if (cancellationSignal != null) {
            selfCancelled = true;
            cancellationSignal.cancel();
            cancellationSignal = null;
        }
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        if (!selfCancelled) {
            showError(errString);
            icon.postDelayed(callback::onError, ERROR_TIMEOUT_MILLIS);
        }
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        showError(helpString);
    }

    @Override
    public void onAuthenticationFailed() {
        showError(icon.getResources().getString(
                R.string.fingerprint_not_recognized));
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        errorTextView.removeCallbacks(mResetErrorTextRunnable);
        icon.setImageResource(R.drawable.ic_fingerprint_success);
        errorTextView.setTextColor(
                errorTextView.getResources().getColor(R.color.success_color, null));
        errorTextView.setText(
                errorTextView.getResources().getString(R.string.fingerprint_success));
        icon.postDelayed(() -> callback.onAuthenticated(), SUCCESS_DELAY_MILLIS);
    }

    private void showError(CharSequence error) {
        icon.setImageResource(R.drawable.ic_fingerprint_error);
        errorTextView.setText(error);
        errorTextView.setTextColor(
                errorTextView.getResources().getColor(R.color.warning_color, null));
        errorTextView.removeCallbacks(mResetErrorTextRunnable);
        errorTextView.postDelayed(mResetErrorTextRunnable, ERROR_TIMEOUT_MILLIS);
    }

    private Runnable mResetErrorTextRunnable = new Runnable() {
        @Override
        public void run() {
            errorTextView.setTextColor(
                    errorTextView.getResources().getColor(R.color.hint_color, null));
            errorTextView.setText(
                    errorTextView.getResources().getString(R.string.fingerprint_hint));
            icon.setImageResource(R.drawable.ic_fp_40px);
        }
    };

    public interface Callback {
        void onAuthenticated();

        void onError();
    }
}